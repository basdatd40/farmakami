from django import forms
from django.db import connection, transaction, IntegrityError


class BaseApotekForm(forms.Form):
    id_apotek = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control',
                                      'readonly': True}),
        max_length=10,
        initial="",
        required=False
    )
    email = forms.EmailField(
        widget=forms.EmailInput(attrs={'class': 'form-control'}),
        max_length=50
    )
    no_sia = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control'}),
        max_length=20
    )
    nama_penyelenggara = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control'}),
        max_length=50
    )
    nama_apotek = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control'}),
        max_length=50
    )
    alamat_apotek = forms.CharField(
        widget=forms.Textarea(attrs={'class': 'form-control',
                                     'rows': 3}),
    )
    telepon_apotek = forms.CharField(
        widget=forms.NumberInput(attrs={
            'class': 'form-control',
            'placeholder': '0xx (w/o country code)'}),
        max_length=20
    )

    def clean_email(self):
        id = self.cleaned_data.get('id_apotek')
        data = self.cleaned_data.get('email')
        with connection.cursor() as cursor:
            cursor.execute("SELECT * FROM APOTEK WHERE id_apotek!=%s AND email=%s",
                           [id, data])
            if cursor.fetchone() is not None:
                raise forms.ValidationError("Email sudah terdaftar")
        return data

    def clean_no_sia(self):
        id = self.cleaned_data.get('id_apotek')
        data = self.cleaned_data.get('no_sia')
        with connection.cursor() as cursor:
            cursor.execute("SELECT * FROM APOTEK WHERE id_apotek!=%s AND no_sia=%s",
                           [id, data])
            if cursor.fetchone() is not None:
                raise forms.ValidationError("Nomor SIA sudah terdaftar")
        return data

    def clean_alamat_apotek(self):
        id = self.cleaned_data.get('id_apotek')
        data = self.cleaned_data.get('alamat_apotek')
        with connection.cursor() as cursor:
            cursor.execute("SELECT * FROM APOTEK WHERE id_apotek!=%s AND alamat_apotek=%s",
                           [id, data])
            if cursor.fetchone() is not None:
                raise forms.ValidationError("Alamat sudah terdaftar")
        return data


class ApotekForm(BaseApotekForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['id_apotek'].required = True
    def save(self):
        cd = self.cleaned_data
        with connection.cursor() as cursor:
            cursor.execute("""
            UPDATE APOTEK SET email = %s, no_sia = %s, nama_penyelenggara = %s,
            nama_apotek = %s, alamat_apotek = %s, telepon_apotek = %s
            WHERE id_apotek=%s
                           """,
                           [cd.get("email"), cd.get("no_sia"),
                            cd.get("nama_penyelenggara"), cd.get("nama_apotek"),
                            cd.get("alamat_apotek"), cd.get("telepon_apotek"),
                            cd.get("id_apotek")])


class NewApotekForm(BaseApotekForm):
    def save(self):
        cd = self.cleaned_data
        with connection.cursor() as cursor:
            cursor.execute("SELECT * FROM APOTEK ORDER BY id_apotek DESC LIMIT 1")
            new_id_number = int(cursor.fetchone()[0][3:]) + 1
            new_id = f"FKA{new_id_number:07}"
            cursor.execute("INSERT INTO APOTEK VALUES(%s, %s, %s, %s, %s, %s, %s)",
                           [new_id, cd.get("email"), cd.get("no_sia"),
                            cd.get("nama_penyelenggara"), cd.get("nama_apotek"),
                            cd.get("alamat_apotek"), cd.get("telepon_apotek"),
                            ])


class BaseProdukForm(forms.Form):
    PRODUK_CHOICES = []
    APOTEK_CHOICES = []

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        with connection.cursor() as cursor:
            cursor.execute("SELECT id_produk, id_produk FROM PRODUK")
            self.fields['id_produk'].choices = cursor.fetchall()
            cursor.execute("SELECT id_apotek, id_apotek FROM APOTEK")
            self.fields['id_apotek'].choices = cursor.fetchall()

    id_produk = forms.ChoiceField(choices=PRODUK_CHOICES,
                                  widget=forms.Select(attrs={'class': 'form-control'}))
    id_apotek = forms.ChoiceField(choices=APOTEK_CHOICES,
                                  widget=forms.Select(attrs={'class': 'form-control'}))
    harga_jual = forms.IntegerField(
        widget=forms.NumberInput(attrs={'class': 'form-control'}))
    stok = forms.IntegerField(
        widget=forms.NumberInput(attrs={'class': 'form-control'}))
    satuan_penjualan = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control'}),
        max_length=5)


class UpdateProdukForm(BaseProdukForm):
    old_id_produk = forms.CharField(
        widget=forms.HiddenInput(attrs={'class': 'form-control',
                                        'readonly': True}),
        max_length=10,
    )
    old_id_apotek = forms.CharField(
        widget=forms.HiddenInput(attrs={'class': 'form-control',
                                        'readonly': True}),
        max_length=10,
    )

    def clean(self):
        old_id_produk = self.cleaned_data.get("old_id_produk")
        old_id_apotek = self.cleaned_data.get("old_id_apotek")
        id_produk = self.cleaned_data.get("id_produk")
        id_apotek = self.cleaned_data.get("id_apotek")
        with connection.cursor() as cursor:
            cursor.execute("SELECT id_produk, id_apotek FROM PRODUK_APOTEK "
                           "WHERE id_produk!=%s AND id_apotek!=%s", [old_id_produk, old_id_apotek])
            existing_data = cursor.fetchall()
        if (id_produk, id_apotek) in existing_data:
            raise forms.ValidationError("Pasangan ID Produk-ID Apotek sudah terdaftar")

    def save(self):
        cd = self.cleaned_data
        try:
            with connection.cursor() as cursor:
                with transaction.atomic():
                    cursor.execute("""\
                    UPDATE produk_apotek SET harga_jual = %s, stok = %s,\
                    satuan_penjualan = %s, id_produk = %s, id_apotek = %s\
                    WHERE id_produk=%s AND id_apotek=%s""",
                                   [cd.get("harga_jual"), cd.get("stok"),
                                    cd.get("satuan_penjualan"), cd.get("id_produk"),
                                    cd.get("id_apotek"), cd.get("old_id_produk"),
                                    cd.get("old_id_apotek")])
        except IntegrityError:
            pass


class NewProdukForm(BaseProdukForm):
    def clean(self):
        id_produk = self.cleaned_data.get("id_produk")
        id_apotek = self.cleaned_data.get("id_apotek")
        with connection.cursor() as cursor:
            cursor.execute("SELECT id_produk, id_apotek FROM PRODUK_APOTEK")
            existing_data = cursor.fetchall()
        if (id_produk, id_apotek) in existing_data:
            raise forms.ValidationError("Pasangan ID Produk-ID Apotek sudah terdaftar")

    def save(self):
        cd = self.cleaned_data
        with connection.cursor() as cursor:
            cursor.execute("INSERT INTO PRODUK_APOTEK VALUES(%s, %s, %s, %s, %s)",
                           [cd.get("harga_jual"), cd.get("stok"),
                            cd.get("satuan_penjualan"), cd.get("id_produk"),
                            cd.get("id_apotek")
                            ])

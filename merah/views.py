from django.shortcuts import render, redirect
from django.db import connection, transaction, IntegrityError
from .forms import *


def user_login_required(function):
    def wrapper(request, *args, **kwargs):
        email = request.session.get('email')
        if email is None:
            return redirect('/login/')
        else:
            return function(request, *args, **kwargs)
    return wrapper


def cursor_fetchall(cursor):
    columns = [col[0] for col in cursor.description]
    return [dict(zip(columns, row)) for row in cursor.fetchall()]


def apotek(request):
    with connection.cursor() as cursor:
        cursor.execute("SELECT * FROM APOTEK ORDER BY id_apotek")
        data_apotek = cursor_fetchall(cursor)

    context = {'data': data_apotek}
    return render(request, "apotek.html", context)


@user_login_required
def update_apotek(request):
    if request.session.get("role") != "ADMIN_APOTEK":
        return redirect('/merah/apotek/')
    if request.method == 'POST':
        form = ApotekForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("/merah/apotek/")
    elif request.method == 'GET':
        if request.GET.get('id_u') is None:
            return redirect('/merah/apotek/')
        with connection.cursor() as cursor:
            cursor.execute("SELECT * FROM APOTEK where id_apotek=%s LIMIT 1;",
                           [request.GET.get('id_u')])
            data = cursor.fetchone()
            if data is None:
                return redirect('/merah/apotek/')
            columns = [col[0] for col in cursor.description]
            data_apotek = dict(zip(columns, data))

        form = ApotekForm(initial=data_apotek)

    context = {'form': form}
    return render(request, "update_apotek.html", context)


@user_login_required
def delete_apotek(request):
    if request.session.get("role") != "ADMIN_APOTEK":
        return redirect('/merah/apotek/')
    if request.method == 'GET':
        if request.GET.get('id_d') is None:
            return redirect('/merah/apotek/')
        else:
            id = request.GET.get('id_d')
            try:
                with connection.cursor() as cursor:
                    with transaction.atomic():
                        cursor.execute("""\
                        DELETE FROM APOTEK WHERE id_apotek=%s\
                        """, [id])
            except IntegrityError:
                pass
            return redirect('/merah/apotek/')


@user_login_required
def create_apotek(request):
    if request.session.get("role") != "ADMIN_APOTEK":
        return redirect('/merah/apotek/')
    if request.method == 'POST':
        form = NewApotekForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("/merah/apotek/")
    if request.method == 'GET':
        form = NewApotekForm()
    context = {'form': form}
    return render(request, "create_apotek.html", context)


def produk_apotek(request):
    with connection.cursor() as cursor:
        cursor.execute("SELECT * FROM PRODUK_APOTEK ORDER BY id_apotek,id_produk")
        data_produk_apotek = cursor_fetchall(cursor)

    context = {'data': data_produk_apotek}
    return render(request, "produk_apotek.html", context)


@user_login_required
def delete_produk_apotek(request):
    if request.session.get("role") != "ADMIN_APOTEK":
        return redirect('/merah/produk_apotek/')
    if request.method == 'GET':
        if None in [request.GET.get('id_d_a'), request.GET.get('id_d_p')]:
            return redirect('/merah/produk_apotek/')
        else:
            try:
                with connection.cursor() as cursor:
                    with transaction.atomic():
                        cursor.execute("""\
                        DELETE FROM PRODUK_APOTEK WHERE id_apotek=%s\
                        AND id_produk=%s
                        """, [request.GET.get('id_d_a'),
                              request.GET.get('id_d_p')])
            except IntegrityError:
                pass
            return redirect('/merah/produk_apotek/')


@user_login_required
def update_produk_apotek(request):
    if request.session.get("role") != "ADMIN_APOTEK":
        return redirect('/merah/produk_apotek/')
    if request.method == 'POST':
        form = UpdateProdukForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("/merah/produk_apotek/")
    elif request.method == 'GET':
        if request.GET.get('id_u_a') is None or request.GET.get('id_u_p') is None:
            return redirect('/merah/produk_apotek/')
        with connection.cursor() as cursor:
            cursor.execute("SELECT id_apotek as old_id_apotek, id_produk as old_id_produk,"
                           "* FROM PRODUK_APOTEK WHERE id_apotek=%s AND id_produk=%s",
                           [request.GET.get('id_u_a'), request.GET.get('id_u_p')])
            columns = [col[0] for col in cursor.description]
            data_produk_apotek = dict(zip(columns, cursor.fetchone()))

        form = UpdateProdukForm(initial=data_produk_apotek)
    context = {'form': form}
    return render(request, "update_produk_apotek.html", context)


@user_login_required
def create_produk_apotek(request):
    if request.session.get("role") != "ADMIN_APOTEK":
        return redirect('/merah/produk_apotek/')
    if request.method == 'POST':
        form = NewProdukForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("/merah/produk_apotek/")
    elif request.method == 'GET':
        form = NewProdukForm()
    context = {'form': form}
    return render(request, "create_produk_apotek.html", context)

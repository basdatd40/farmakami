from django.urls import path
from .views import *

urlpatterns = [
    path("produk_apotek/create/", create_produk_apotek, name="create_produk_apotek"),
    path("produk_apotek/update/", update_produk_apotek, name="update_produk_apotek"),
    path("produk_apotek/delete/", delete_produk_apotek, name="delete_produk_apotek"),
    path("produk_apotek/", produk_apotek, name="produk_apotek"),
    path("apotek/create/", create_apotek, name="create_apotek"),
    path("apotek/delete/", delete_apotek, name="delete_apotek"),
    path("apotek/update/", update_apotek, name="update_apotek"),
    path("apotek/", apotek, name="apotek")
]

from django.db import connection
from django.test import TestCase
from django.urls import resolve

import os

from farmakami.settings import BASE_DIR
from merah.forms import *
from merah.views import *


# Create your tests here.
class FKMerahTestCases(TestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        with connection.cursor() as cursor:
            with open(os.path.join(
                    BASE_DIR,
                    'assets',
                    'sql',
                    'create_sqlite.sql'), 'r') as f:
                for statement in f.read().split("--"):
                    cursor.execute(statement)

    def test_read_apotek_page_url_valid(self):
        response = self.client.get("/merah/apotek/")
        self.assertEqual(response.status_code, 200)

    def test_read_apotek_page_function_used(self):
        response = resolve("/merah/apotek/")
        self.assertEqual(response.func, apotek)

    def test_read_apotek_page_template_used(self):
        response = self.client.get("/merah/apotek/")
        self.assertTemplateUsed(response, "apotek.html")

    def test_update_apotek_url_no_argument_redirect(self):
        self.client.post("/login/", data={'email': 'b@pk.com', 'password': '456'})
        response = self.client.get("/merah/apotek/update/")
        self.assertRedirects(response, '/merah/apotek/')

    def test_update_apotek_wrong_permission_url_with_argument_redirect(self):
        self.client.post("/login/", data={'email': 'a@pk.com', 'password': '123'})
        response = self.client.get("/merah/apotek/update/", data={
            'id_u': 'FKA0000001'
        })
        self.assertRedirects(response, '/merah/apotek/')

    def test_update_apotek_not_logged_in_url_with_argument_valid(self):
        response = self.client.get("/merah/apotek/update/", data={
            'id_u': 'FKA0000001'
        })
        self.assertRedirects(response, '/login/')

    def test_update_apotek_permission_ok_url_with_argument_valid(self):
        self.client.post("/login/", data={'email': 'b@pk.com', 'password': '456'})
        response = self.client.get("/merah/apotek/update/", data={
            'id_u': 'FKA0000001'
        })
        self.assertEqual(response.status_code, 200)

    def test_update_apotek_wrong_id_with_argument_valid(self):
        self.client.post("/login/", data={'email': 'b@pk.com', 'password': '456'})
        response = self.client.get("/merah/apotek/update/", data={
            'id_u': 'FKA00000006'
        })
        self.assertRedirects(response, "/merah/apotek/")

    def test_update_apotek_function_used(self):
        response = resolve("/merah/apotek/update/")
        self.assertEqual(response.func, update_apotek)

    def test_update_apotek_permission_ok_with_argument_template_used(self):
        self.client.post("/login/", data={'email': 'b@pk.com', 'password': '456'})
        response = self.client.get("/merah/apotek/update/", data={
            'id_u': 'FKA0000001'
        })
        self.assertTemplateUsed(response, "update_apotek.html")

    def test_form_update_apotek_valid_values(self):
        form = ApotekForm({
            'id_apotek': 'FKA0000001',
            'email': 'cc@pk.com',
            'no_sia': 'SIADDDDDDDD',
            'nama_penyelenggara': 'HAHA',
            'nama_apotek': 'HIHI',
            'alamat_apotek': 'LILI',
            'telepon_apotek': '2329392392'
        })
        self.assertTrue(form.is_valid())

    def test_form_update_apotek_too_long(self):
        form = ApotekForm({
            'id_apotek': 'F'*100,
            'email': 'A'*100,
            'no_sia': 'S'*100,
            'nama_penyelenggara': 'H'*100,
            'nama_apotek': 'I'*100,
            'alamat_apotek': 'L'*100,
            'telepon_apotek': '2'*100
        })
        self.assertFalse(form.is_valid())

    def test_form_update_apotek_existing_email(self):
        form = ApotekForm({
            'id_apotek': 'FKA0000001',
            'email': 'pharmaparlor@fk.com',
            'no_sia': 'SIADDDDDDDD',
            'nama_penyelenggara': 'HAHA',
            'nama_apotek': 'HIHI',
            'alamat_apotek': 'LILI',
            'telepon_apotek': '22293293'
        })
        self.assertFalse(form.is_valid())

    def test_form_update_apotek_existing_no_sia(self):
        form = ApotekForm({
            'id_apotek': 'FKA0000001',
            'email': 'cc@pk.com',
            'no_sia': 'SIA205217338890',
            'nama_penyelenggara': 'HAHA',
            'nama_apotek': 'HIHI',
            'alamat_apotek': 'LILI',
            'telepon_apotek': '12121929128'
        })
        self.assertFalse(form.is_valid())

    def test_form_update_apotek_existing_address(self):
        form = ApotekForm({
            'id_apotek': 'FKA0000001',
            'email': 'cc@pk.com',
            'no_sia': 'SIADDDDDDDD',
            'nama_penyelenggara': 'HAHA',
            'nama_apotek': 'HIHI',
            'alamat_apotek': 'Jl Menanggal Utr WP-02, Jawa Timur',
            'telepon_apotek': '00202020'
        })
        self.assertFalse(form.is_valid())

    def test_update_apotek_valid_redirect_to_stub(self):
        self.client.post("/login/", data={'email': 'b@pk.com', 'password': '456'})
        response = self.client.post('/merah/apotek/update/', {
            'id_apotek': 'FKA0000001',
            'email': 'cc@pk.com',
            'no_sia': 'SIADDDDDDDD',
            'nama_penyelenggara': 'HAHA',
            'nama_apotek': 'HIHI',
            'alamat_apotek': 'LILI',
            'telepon_apotek': '202020020'
        })
        self.assertRedirects(response, "/merah/apotek/")

    def test_delete_apotek_url_no_argument_redirect(self):
        self.client.post("/login/", data={'email': 'b@pk.com', 'password': '456'})
        response = self.client.get("/merah/apotek/delete/")
        self.assertRedirects(response, '/merah/apotek/')

    def test_delete_apotek_wrong_permission_url_with_argument_redirect(self):
        self.client.post("/login/", data={'email': 'a@pk.com', 'password': '123'})
        response = self.client.get("/merah/apotek/delete/", data={
            'id_d': 'FKA0000001'
        })
        self.assertRedirects(response, '/merah/apotek/')

    def test_delete_apotek_not_logged_in_url_with_argument_valid(self):
        response = self.client.get("/merah/apotek/delete/", data={
            'id_d': 'FKA0000001'
        })
        self.assertRedirects(response, '/login/')

    def test_delete_apotek_permission_ok_url_with_argument_redirect_no_fk_violation(self):
        self.client.post("/login/", data={'email': 'b@pk.com', 'password': '456'})
        response = self.client.get("/merah/apotek/delete/", data={
            'id_d': 'FKA0000005'
        })
        with connection.cursor() as cursor:
            cursor.execute("SELECT * FROM APOTEK where id_apotek=%s", ['FKA0000005'])
            self.assertIsNone(cursor.fetchone())
        self.assertRedirects(response, '/merah/apotek/')

    def test_delete_apotek_permission_ok_url_with_argument_redirect_fk_violation(self):
        self.client.post("/login/", data={'email': 'b@pk.com', 'password': '456'})
        response = self.client.get("/merah/apotek/delete/", data={
            'id_d': 'FKA0000001'
        })
        with connection.cursor() as cursor:
            cursor.execute("SELECT * FROM APOTEK where id_apotek=%s", ['FKA0000001'])
            self.assertIsNotNone(cursor.fetchone())
        self.assertRedirects(response, '/merah/apotek/')

    def test_delete_apotek_function_used(self):
        response = resolve("/merah/apotek/delete/")
        self.assertEqual(response.func, delete_apotek)

    def test_create_apotek_logged_in_ok(self):
        self.client.post("/login/", data={'email': 'b@pk.com', 'password': '456'})
        response = self.client.get("/merah/apotek/create/")
        self.assertEqual(response.status_code, 200)

    def test_create_apotek_wrong_permission_redirect(self):
        self.client.post("/login/", data={'email': 'a@pk.com', 'password': '123'})
        response = self.client.get("/merah/apotek/create/")
        self.assertRedirects(response, '/merah/apotek/')

    def test_create_apotek_not_logged_in_redirect(self):
        response = self.client.get("/merah/apotek/create/")
        self.assertRedirects(response, '/login/')

    def test_create_apotek_function_used(self):
        response = resolve("/merah/apotek/create/")
        self.assertEqual(response.func, create_apotek)

    def test_create_apotek_logged_in_template_used(self):
        self.client.post("/login/", data={'email': 'b@pk.com', 'password': '456'})
        response = self.client.get("/merah/apotek/create/")
        self.assertTemplateUsed(response, "create_apotek.html")

    def test_form_create_apotek_valid_values(self):
        form = NewApotekForm({
            'email': 'dddd@pk.com',
            'no_sia': 'SIANNNHHSHAHS',
            'nama_penyelenggara': 'HEHE',
            'nama_apotek': 'HUHU',
            'alamat_apotek': 'LALA',
            'telepon_apotek': '0188228228'
        })
        self.assertTrue(form.is_valid())

    def test_form_create_apotek_too_long(self):
        form = NewApotekForm({
            'email': 'A'*100,
            'no_sia': 'S'*100,
            'nama_penyelenggara': 'H'*100,
            'nama_apotek': 'I'*100,
            'alamat_apotek': 'L'*100,
            'telepon_apotek': '2'*100
        })
        self.assertFalse(form.is_valid())

    def test_form_create_apotek_existing_email(self):
        form = NewApotekForm({
            'email': 'pharmaparlor@fk.com',
            'no_sia': 'SIANNNHHSHAHS',
            'nama_penyelenggara': 'HEHE',
            'nama_apotek': 'HUHU',
            'alamat_apotek': 'LALA',
            'telepon_apotek': '0188228228'
        })
        self.assertFalse(form.is_valid())

    def test_form_create_apotek_existing_no_sia(self):
        form = NewApotekForm({
            'email': 'dddd@pk.com',
            'no_sia': 'SIA205217338890',
            'nama_penyelenggara': 'HEHE',
            'nama_apotek': 'HUHU',
            'alamat_apotek': 'LALA',
            'telepon_apotek': '0188228228'
        })
        self.assertFalse(form.is_valid())

    def test_form_create_apotek_existing_address(self):
        form = NewApotekForm({
            'email': 'dddd@pk.com',
            'no_sia': 'SIANNNHHSHAHS',
            'nama_penyelenggara': 'HEHE',
            'nama_apotek': 'HUHU',
            'alamat_apotek': 'Jl Menanggal Utr WP-02, Jawa Timur',
            'telepon_apotek': '0188228228'
        })
        self.assertFalse(form.is_valid())

    def test_form_create_apotek_post_ok_redirect(self):
        self.client.post("/login/", data={'email': 'b@pk.com', 'password': '456'})
        response = self.client.post("/merah/apotek/create/", {
            'email': 'dddd@pk.com',
            'no_sia': 'SIANNNHHSHAHS',
            'nama_penyelenggara': 'HEHE',
            'nama_apotek': 'HUHU',
            'alamat_apotek': 'LALA',
            'telepon_apotek': '0188228228'
        })
        with connection.cursor() as cursor:
            cursor.execute("SELECT * FROM apotek WHERE no_sia=%s LIMIT 1",
                           ['SIANNNHHSHAHS'])
            self.assertIsNotNone(cursor.fetchone())
        self.assertRedirects(response, "/merah/apotek/")

    def test_read_produk_apotek_page_url_valid(self):
        response = self.client.get("/merah/produk_apotek/")
        self.assertEqual(response.status_code, 200)

    def test_read_produk_apotek_page_function_used(self):
        response = resolve("/merah/produk_apotek/")
        self.assertEqual(response.func, produk_apotek)

    def test_read_produk_apotek_page_template_used(self):
        response = self.client.get("/merah/produk_apotek/")
        self.assertTemplateUsed(response, "produk_apotek.html")

    def test_delete_produk_apotek_url_no_argument_redirect(self):
        self.client.post("/login/", data={'email': 'b@pk.com', 'password': '456'})
        response = self.client.get("/merah/produk_apotek/delete/")
        self.assertRedirects(response, '/merah/produk_apotek/')

    def test_delete_produk_apotek_wrong_permission_url_with_argument_redirect(self):
        self.client.post("/login/", data={'email': 'a@pk.com', 'password': '123'})
        response = self.client.get("/merah/produk_apotek/delete/", data={
            'id_d_a': 'FKA0000001',
            'id_d_p': 'FPK0000001'
        })
        self.assertRedirects(response, '/merah/produk_apotek/')

    def test_delete_produk_apotek_not_logged_in_url_with_argument_valid(self):
        response = self.client.get("/merah/produk_apotek/delete/", data={
            'id_d_a': 'FKA0000001',
            'id_d_p': 'FPK0000001'
        })
        self.assertRedirects(response, '/login/')

    def test_delete_produk_apotek_permission_ok_url_fk_violation(self):
        self.client.post("/login/", data={'email': 'b@pk.com', 'password': '456'})
        response = self.client.get("/merah/produk_apotek/delete/", data={
            'id_d_a': 'FKA0000001',
            'id_d_p': 'FPK0000001'
        })
        with connection.cursor() as cursor:
            cursor.execute("SELECT * FROM produk_apotek WHERE id_apotek=%s AND\
                           id_produk=%s", ['FKA0000001', 'FPK0000001'])
            self.assertIsNotNone(cursor.fetchone())
        self.assertRedirects(response, '/merah/produk_apotek/')

    def test_delete_produk_apotek_permission_ok_url_no_fk_violation(self):
        self.client.post("/login/", data={'email': 'b@pk.com', 'password': '456'})
        response = self.client.get("/merah/produk_apotek/delete/", data={
            'id_d_a': 'FKA0000002',
            'id_d_p': 'FPK0000002'
        })
        with connection.cursor() as cursor:
            cursor.execute("SELECT * FROM produk_apotek WHERE id_apotek=%s AND\
                           id_produk=%s", ['FKA0000002', 'FPK0000002'])
            self.assertIsNone(cursor.fetchone())
        self.assertRedirects(response, '/merah/produk_apotek/')

    def test_delete_produk_apotek_function_used(self):
        response = resolve("/merah/produk_apotek/delete/")
        self.assertEqual(response.func, delete_produk_apotek)
        
    def test_update_produk_apotek_url_no_argument_redirect(self):
        self.client.post("/login/", data={'email': 'b@pk.com', 'password': '456'})
        response = self.client.get("/merah/produk_apotek/update/")
        self.assertRedirects(response, '/merah/produk_apotek/')

    def test_update_produk_apotek_wrong_permission_url_with_argument_redirect(self):
        self.client.post("/login/", data={'email': 'a@pk.com', 'password': '123'})
        response = self.client.get("/merah/produk_apotek/update/", data={
            'id_u_a': 'FKA0000001',
            'id_u_p': 'FPK0000001'
        })
        self.assertRedirects(response, '/merah/produk_apotek/')

    def test_update_produk_apotek_not_logged_in_url_with_argument_valid(self):
        response = self.client.get("/merah/produk_apotek/update/", data={
            'id_u_a': 'FKA0000001',
            'id_u_p': 'FPK0000001'
        })
        self.assertRedirects(response, '/login/')

    def test_update_produk_apotek_permission_ok_url_with_argument_valid(self):
        self.client.post("/login/", data={'email': 'b@pk.com', 'password': '456'})
        response = self.client.get("/merah/produk_apotek/update/", data={
            'id_u_a': 'FKA0000001',
            'id_u_p': 'FPK0000001'
        })
        self.assertEqual(response.status_code, 200)

    def test_update_produk_apotek_function_used(self):
        response = resolve("/merah/produk_apotek/update/")
        self.assertEqual(response.func, update_produk_apotek)

    def test_update_produk_apotek_permission_ok_with_argument_template_used(self):
        self.client.post("/login/", data={'email': 'b@pk.com', 'password': '456'})
        response = self.client.get("/merah/produk_apotek/update/", data={
            'id_u_a': 'FKA0000001',
            'id_u_p': 'FPK0000001'
        })
        self.assertTemplateUsed(response, "update_produk_apotek.html")

    def test_form_update_produk_apotek_all_valid_values(self):
        form = UpdateProdukForm({
            'old_id_apotek': 'FKA0000001',
            'old_id_produk': 'FPK0000001',
            'id_apotek': 'FKA0000003',
            'id_produk': 'FPK0000003',
            'harga_jual': 100000,
            'stok': 500,
            'satuan_penjualan': 'ABCD',

        })
        self.assertTrue(form.is_valid())

    def test_form_update_produk_apotek_too_long(self):
        form = UpdateProdukForm({
            'old_id_apotek': 'FKA0000001',
            'old_id_produk': 'FPK0000001',
            'id_apotek': 'F'*100,
            'id_produk': 'F'*100,
            'harga_jual': 100000,
            'stok': 500,
            'satuan_penjualan': 'A'*50,

        })
        self.assertFalse(form.is_valid())

    def test_form_update_produk_apotek_existing_ap_pair(self):
        form = UpdateProdukForm({
            'old_id_apotek': 'FKA0000001',
            'old_id_produk': 'FPK0000001',
            'id_apotek': 'FKA0000002',
            'id_produk': 'FPK0000002',
            'harga_jual': 100000,
            'stok': 500,
            'satuan_penjualan': 'ABCD',

        })
        self.assertFalse(form.is_valid())

    def test_form_update_produk_apotek_post_fk_violation(self):
        self.client.post("/login/", data={'email': 'b@pk.com', 'password': '456'})
        response = self.client.post("/merah/produk_apotek/update/", {
            'old_id_apotek': 'FKA0000001',
            'old_id_produk': 'FPK0000001',
            'id_apotek': 'FKA0000003',
            'id_produk': 'FPK0000003',
            'harga_jual': 100000,
            'stok': 500,
            'satuan_penjualan': 'ABCD',
        })
        with connection.cursor() as cursor:
            cursor.execute("SELECT * FROM produk_apotek WHERE id_apotek=%s\
                           AND id_produk=%s",
                           ['FKA0000003', 'FPK0000003'])
            self.assertIsNone(cursor.fetchone())
        self.assertRedirects(response, "/merah/produk_apotek/")

    def test_form_update_produk_apotek_post_no_fk_violation(self):
        self.client.post("/login/", data={'email': 'b@pk.com', 'password': '456'})
        response = self.client.post("/merah/produk_apotek/update/", {
            'old_id_apotek': 'FKA0000002',
            'old_id_produk': 'FPK0000002',
            'id_apotek': 'FKA0000003',
            'id_produk': 'FPK0000003',
            'harga_jual': 100000,
            'stok': 500,
            'satuan_penjualan': 'ABCD',
        })
        with connection.cursor() as cursor:
            cursor.execute("SELECT * FROM produk_apotek WHERE id_apotek=%s\
                           AND id_produk=%s",
                           ['FKA0000003', 'FPK0000003'])
            self.assertIsNotNone(cursor.fetchone())
        self.assertRedirects(response, "/merah/produk_apotek/")

    def test_create_produk_apotek_logged_in_ok(self):
        self.client.post("/login/", data={'email': 'b@pk.com', 'password': '456'})
        response = self.client.get("/merah/produk_apotek/create/")
        self.assertEqual(response.status_code, 200)

    def test_create_produk_apotek_wrong_permission_redirect(self):
        self.client.post("/login/", data={'email': 'a@pk.com', 'password': '123'})
        response = self.client.get("/merah/produk_apotek/create/")
        self.assertRedirects(response, '/merah/produk_apotek/')

    def test_create_produk_apotek_not_logged_in_redirect(self):
        response = self.client.get("/merah/produk_apotek/create/")
        self.assertRedirects(response, '/login/')

    def test_create_produk_apotek_function_used(self):
        response = resolve("/merah/produk_apotek/create/")
        self.assertEqual(response.func, create_produk_apotek)

    def test_create_produk_apotek_logged_in_template_used(self):
        self.client.post("/login/", data={'email': 'b@pk.com', 'password': '456'})
        response = self.client.get("/merah/produk_apotek/create/")
        self.assertTemplateUsed(response, "create_produk_apotek.html")

    def test_form_create_produk_apotek_all_valid_values(self):
        form = NewProdukForm({
            'id_apotek': 'FKA0000005',
            'id_produk': 'FPK0000005',
            'harga_jual': 100000,
            'stok': 500,
            'satuan_penjualan': 'ABCD',

        })
        self.assertTrue(form.is_valid())

    def test_form_create_produk_apotek_too_long(self):
        form = NewProdukForm({
            'id_apotek': 'F'*100,
            'id_produk': 'F'*100,
            'harga_jual': 100000,
            'stok': 500,
            'satuan_penjualan': 'A'*50,

        })
        self.assertFalse(form.is_valid())

    def test_form_create_produk_apotek_existing_ap_pair(self):
        form = NewProdukForm({
            'id_apotek': 'FKA0000002',
            'id_produk': 'FPK0000002',
            'harga_jual': 100000,
            'stok': 500,
            'satuan_penjualan': 'ABCD',

        })
        self.assertFalse(form.is_valid())

    def test_form_create_produk_apotek_post_ok_redirect(self):
        self.client.post("/login/", data={'email': 'b@pk.com', 'password': '456'})
        response = self.client.post("/merah/produk_apotek/create/", {
            'id_apotek': 'FKA0000005',
            'id_produk': 'FPK0000005',
            'harga_jual': 100000,
            'stok': 500,
            'satuan_penjualan': 'ABCD',
        })
        with connection.cursor() as cursor:
            cursor.execute("SELECT * FROM produk_apotek WHERE id_apotek=%s AND id_produk=%s",
                           ['FKA0000005', 'FPK0000005'])
            self.assertIsNotNone(cursor.fetchone())
        self.assertRedirects(response, "/merah/produk_apotek/")

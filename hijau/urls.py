from django.urls import path, include
from .views import *

app_name = 'hijau'
urlpatterns = [
    path('createtp', CreateTP, name="createtp"),
    path('viewtp', ViewTP, name="viewtp"),
    path('updatetp/<id>/', UpdateTP, name="updatetp"),
    path('deletetp/<id>/', DeleteTP, name="deletetp")

]
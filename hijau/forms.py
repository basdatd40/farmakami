from django import forms
from django.db import connection

class TPForms(forms.Form):
    id_transaksi_pembelian = forms.CharField(label = 'ID Transaksi Pembelian', 
        max_length = 10, widget=forms.TextInput(attrs={'class' : 'form-idtp'}))

    waktu_pembelian = forms.DateTimeField(label = 'Waktu Pembelian', 
        required = True, widget = forms.DateTimeInput(attrs = {"class": "form-tanggal", "type":"date"}))
        
    total_pembayaran = forms.IntegerField(label = 'Total Pembayaran', 
        widget=forms.NumberInput(attrs={'class' : 'form-tp'}))

    id_konsumen = forms.CharField(label = 'ID Konsumen', 
        max_length = 10, widget=forms.TextInput(attrs={'class' : 'form-idk'}))
        
class CreateForms(forms.Form):
    ID_KONSUMEN_CHOICES = []

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        with connection.cursor() as cursor:
            cursor.execute("SELECT id_konsumen, id_konsumen FROM KONSUMEN")
            self.fields['id_konsumen'].choices = cursor.fetchall()
    
    id_konsumen= forms.ChoiceField(choices=ID_KONSUMEN_CHOICES, label='ID Konsumen', widget=forms.Select(attrs= {"class":"form-control"}))
from django.shortcuts import render, redirect
from .forms import TPForms, CreateForms
from merah.views import user_login_required

from django.db import connection
from django.db import IntegrityError
from collections import namedtuple
import datetime
import os
# Create your views here.

#From Django Docs
def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

# @user_login_required
def ViewTP(request):
    role = request.session['role']
    dataset = ''
    with connection.cursor() as cursor:
        if role =='KONSUMEN':
            cursor.execute("SELECT * FROM KONSUMEN WHERE email=%s", [request.session.get("email")])
            dataset = namedtuplefetchall(cursor)
            cursor.execute("SELECT * FROM TRANSAKSI_PEMBELIAN WHERE id_konsumen='" + dataset[0].id_konsumen + "'")
            dataset = namedtuplefetchall(cursor)
            context = {'table': dataset}
        else:
            cursor.execute("SELECT * FROM TRANSAKSI_PEMBELIAN")
            dataset = namedtuplefetchall(cursor)
            context = {'table': dataset}
        return render(request,"viewTP.html", context)

@user_login_required
def UpdateTP(request, id):
    with connection.cursor() as cursor:
        if request.method == 'POST':
            form = CreateForms(request.POST)
            if form.is_valid():
                id_konsumen = form.cleaned_data['id_konsumen']
                waktu_pembelian = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
                id_transaksi_pembelian = request.POST.get("id_transaksi_pembelian")
                if id_konsumen != id:
                    cursor.execute("UPDATE TRANSAKSI_PEMBELIAN SET id_konsumen =%s where id_transaksi_pembelian =%s", [id_konsumen, id])
                    cursor.execute("UPDATE TRANSAKSI_PEMBELIAN SET waktu_pembelian=%s where id_transaksi_pembelian=%s", [waktu_pembelian, id])
                return redirect('/hijau/viewtp')
        cursor.execute("SELECT waktu_pembelian FROM TRANSAKSI_PEMBELIAN where id_transaksi_pembelian=%s", [id])
        waktu_pembelian = cursor.fetchone()[0]

        cursor.execute("SELECT total_pembayaran FROM TRANSAKSI_PEMBELIAN where id_transaksi_pembelian=%s", [id])
        total_pembayaran = cursor.fetchone()[0]

        form = CreateForms()
        context = {'form': form, 'id':id, 'waktu_pembelian':waktu_pembelian, 'total_pembayaran':total_pembayaran}
        return render(request, "updateTP.html", context)

@user_login_required
def DeleteTP(request, id):
    with connection.cursor() as cursor:
        try:
            cursor.execute("DELETE FROM TRANSAKSI_PEMBELIAN WHERE id_transaksi_pembelian=%s",[id])
        except IntegrityError:
            pass
        return redirect('/hijau/viewtp')

@user_login_required
def CreateTP(request):
    with connection.cursor() as cursor:
        if request.method =='POST':
            form = CreateForms(request.POST)
            if form.is_valid():
                id_konsumen = form.cleaned_data['id_konsumen']
                waktu_pembelian = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
                cursor.execute("SELECT COUNT(*) FROM TRANSAKSI_PEMBELIAN")
                jumlahid = cursor.fetchone()[0]
                id_transaksi_baru = jumlahid+1
                cursor.execute("INSERT INTO TRANSAKSI_PEMBELIAN VALUES (%s, %s, %s, %s)", 
                                [id_transaksi_baru, waktu_pembelian, 0, id_konsumen])
        form = CreateForms()
        context = {'form':form}
        return render(request, 'createTP.html', context)



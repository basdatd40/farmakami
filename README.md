# Farmakami

[![pipeline status](https://gitlab.com/basdatd40/farmakami/badges/master/pipeline.svg)](https://gitlab.com/basdatd40/farmakami/-/commits/master)
[![coverage report](https://gitlab.com/basdatd40/farmakami/badges/master/coverage.svg)](https://gitlab.com/basdatd40/farmakami/-/commits/master)
[![License](https://img.shields.io/badge/License-BSD%203--Clause-blue.svg)](https://opensource.org/licenses/BSD-3-Clause)

A web-based online marketplace for medical products. This simple project is used as a database proof-of-concept. This project is part of the Database (CSGE602070) course at Fasilkom UI.

## Authors

Group K40, Class D, Database (CSGE602070) course, Even Semester 2019/2020

1. [Muhammad Akbar Rafsanjhani](https://gitlab.com/akbarrafs)
2. [Nadia Adilah Rahimullah](https://gitlab.com/catsa)
3. [Rafi Muhammad Daffa](https://gitlab.com/skycruiser8)
4. [Roy Godsend Salomo](https://gitlab.com/roygodsend2301)
5. [Yasmin Adelia Puti Chaidir](https://gitlab.com/yasminadelia)

## Requirements

This project is built on top of Python 3. To ensure full compatibility, please use the latest Python version. Package requirements are specified by the included requirements.txt file.

To use the requirements.txt file, use the following command:

~~~shell
pip install -r requirements.txt
~~~

## Component Branch Statuses

1. Authentication: [![pipeline status](https://gitlab.com/basdatd40/farmakami/badges/rafi-auth/pipeline.svg)](https://gitlab.com/basdatd40/farmakami/-/commits/rafi-auth)
2. Biru: [![pipeline status](https://gitlab.com/basdatd40/farmakami/badges/yasmin/pipeline.svg)](https://gitlab.com/basdatd40/farmakami/-/commits/yasmin)
3. Hijau: [![pipeline status](https://gitlab.com/basdatd40/farmakami/badges/branchBarre/pipeline.svg)](https://gitlab.com/basdatd40/farmakami/-/commits/branchBarre)
4. Merah: [![pipeline status](https://gitlab.com/basdatd40/farmakami/badges/rafi-merah/pipeline.svg)](https://gitlab.com/basdatd40/farmakami/-/commits/rafi-merah)
5. Kuning: [![pipeline status](https://gitlab.com/basdatd40/farmakami/badges/nadia/pipeline.svg)](https://gitlab.com/basdatd40/farmakami/-/commits/nadia)
6. Ungu: [![pipeline status](https://gitlab.com/basdatd40/farmakami/badges/roy/pipeline.svg)](https://gitlab.com/basdatd40/farmakami/-/commits/roy)

## License

[![License](https://img.shields.io/badge/License-BSD%203--Clause-blue.svg)](https://opensource.org/licenses/BSD-3-Clause)

Copyright (c) 2020, Faculty of Computer Science Universitas Indonesia

Permission to copy, modify, and share the works in this project are governed under the [BSD 3-Clause](https://opensource.org/licenses/BSD-3-Clause) license.

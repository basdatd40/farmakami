CREATE TABLE PENGGUNA(
    email VARCHAR(50),
    telepon VARCHAR(20),
    password VARCHAR(128) NOT NULL,
    nama_lengkap VARCHAR(50) NOT NULL,
    PRIMARY KEY (email)
);
--
CREATE TABLE KONSUMEN(
    id_konsumen VARCHAR(10),
    email VARCHAR(50) NOT NULL,
    jenis_kelamin VARCHAR(1) NOT NULL,
    tanggal_lahir DATE NOT NULL,
    PRIMARY KEY (id_konsumen),
    FOREIGN KEY (email) REFERENCES
    PENGGUNA(email) ON UPDATE CASCADE ON DELETE CASCADE
);
--
CREATE TABLE ALAMAT_KONSUMEN(
    id_konsumen VARCHAR(10),
    alamat TEXT,
    status VARCHAR(20),
    PRIMARY KEY (id_konsumen, alamat, status),
    FOREIGN KEY (id_konsumen) REFERENCES
    KONSUMEN(id_konsumen) ON UPDATE CASCADE ON DELETE CASCADE
);
--
CREATE TABLE APOTEKER(
    email VARCHAR(50),
    PRIMARY KEY (email),
    FOREIGN KEY (email) REFERENCES
    PENGGUNA (email)
);
--
CREATE TABLE CS(
    no_ktp VARCHAR(20),
    email VARCHAR(50) NOT NULL,
    no_sia VARCHAR(20) NOT NULL UNIQUE,
    PRIMARY KEY (no_ktp),
    FOREIGN KEY (email) REFERENCES
    APOTEKER(email)
);
--
CREATE TABLE APOTEK(
    id_apotek VARCHAR(10),
    email VARCHAR(50) NOT NULL UNIQUE,
    no_sia VARCHAR(20) NOT NULL UNIQUE,
    nama_penyelenggara VARCHAR(50) NOT NULL,
    nama_apotek VARCHAR(50) NOT NULL,
    alamat_apotek TEXT NOT NULL UNIQUE,
    telepon_apotek VARCHAR(20),
    PRIMARY KEY (id_apotek)
);
--
CREATE TABLE ADMIN_APOTEK(
    email VARCHAR(50),
    id_apotek VARCHAR(10),
    PRIMARY KEY (email),
    FOREIGN KEY (email) REFERENCES
    APOTEKER(email),
    FOREIGN KEY (id_apotek) REFERENCES
    APOTEK (id_apotek)
);
--
CREATE TABLE KURIR(
    id_kurir VARCHAR(10),
    email VARCHAR(50) NOT NULL,
    nama_perusahaan VARCHAR(50) NOT NULL,
    PRIMARY KEY (id_kurir),
    FOREIGN KEY (email) REFERENCES
    PENGGUNA (email)
);
--
CREATE TABLE TRANSAKSI_PEMBELIAN(
    id_transaksi_pembelian VARCHAR(10),
    waktu_pembelian TIMESTAMP NOT NULL,
    total_pembayaran INTEGER NOT NULL,
    id_konsumen VARCHAR(10) NOT NULL,
    PRIMARY KEY (id_transaksi_pembelian),
    FOREIGN KEY (id_konsumen) REFERENCES
    KONSUMEN (id_konsumen)
);
--
CREATE TABLE TRANSAKSI_DENGAN_RESEP(
    no_resep VARCHAR(20),
    nama_dokter VARCHAR(50) NOT NULL,
    nama_pasien VARCHAR(50) NOT NULL,
    isi_resep TEXT NOT NULL,
    unggahan_resep TEXT,
    tanggal_resep DATE NOT NULL,
    status_validasi VARCHAR(20) NOT NULL,
    no_telepon VARCHAR(20),
    id_transaksi_pembelian VARCHAR(10) NOT NULL,
    email_apoteker VARCHAR(50),
    PRIMARY KEY(no_resep),
    FOREIGN KEY (id_transaksi_pembelian) REFERENCES
    TRANSAKSI_PEMBELIAN (id_transaksi_pembelian),
    FOREIGN KEY (email_apoteker) REFERENCES
    APOTEKER (email)
);
--
CREATE TABLE TRANSAKSI_ONLINE(
    no_urut_pembelian VARCHAR(20),
    id_transaksi_pembelian VARCHAR(10),
    PRIMARY KEY (no_urut_pembelian),
    FOREIGN KEY (id_transaksi_pembelian) REFERENCES
    TRANSAKSI_PEMBELIAN (id_transaksi_pembelian)
);
--
CREATE TABLE PRODUK(
    id_produk VARCHAR(10),
    PRIMARY KEY (id_produk)
);
--
CREATE TABLE PRODUK_APOTEK(
    harga_jual INTEGER NOT NULL,
    stok INTEGER NOT NULL,
    satuan_penjualan varchar(5) NOT NULL,
    id_produk VARCHAR(10),
    id_apotek VARCHAR(10),
    PRIMARY KEY (id_produk, id_apotek),
    FOREIGN KEY (id_produk) REFERENCES
    PRODUK (id_produk),
    FOREIGN KEY (id_apotek) REFERENCES
    APOTEK (id_apotek)
);
--
CREATE TABLE LIST_PRODUK_DIBELI(
    jumlah INTEGER NOT NULL,
    id_apotek VARCHAR(10),
    id_produk VARCHAR(10),
    id_transaksi_pembelian VARCHAR(10),
    PRIMARY KEY (id_apotek, id_produk, id_transaksi_pembelian),
    FOREIGN KEY (id_produk, id_apotek) REFERENCES
    PRODUK_APOTEK (id_produk, id_apotek),
    FOREIGN KEY (id_transaksi_pembelian) REFERENCES
    TRANSAKSI_PEMBELIAN (id_transaksi_pembelian)
);
--
CREATE TABLE ALAT_MEDIS(
    id_alat_medis VARCHAR(10),
    nama_alat_medis VARCHAR(50) NOT NULL,
    deskripsi_alat TEXT,
    jenis_penggunaan VARCHAR(20) NOT NULL,
    id_produk VARCHAR(10) NOT NULL,
    PRIMARY KEY (id_alat_medis),
    FOREIGN KEY (id_produk) REFERENCES
    PRODUK (id_produk)
);
--
CREATE TABLE MERK_DAGANG(
    id_merk VARCHAR(10),
    nama_merk VARCHAR(50) NOT NULL,
    perusahaan VARCHAR(50) NOT NULL,
    PRIMARY KEY (id_merk)
);
--
CREATE TABLE MERK_DAGANG_ALAT_MEDIS(
    id_alat_medis VARCHAR(10),
    id_merk VARCHAR(10),
    fitur TEXT,
    dimensi VARCHAR(10) NOT NULL,
    cara_penggunaan TEXT,
    model VARCHAR(20),
    PRIMARY KEY (id_alat_medis, id_merk, fitur),
    FOREIGN KEY (id_alat_medis) REFERENCES
    ALAT_MEDIS (id_alat_medis),
    FOREIGN KEY (id_merk) REFERENCES
    MERK_DAGANG(id_merk)
);
--
CREATE TABLE MERK_OBAT(
    id_merk_obat VARCHAR(10),
    golongan VARCHAR(25) NOT NULL,
    nama_dagang VARCHAR(50) NOT NULL,
    perusahaan VARCHAR(50) NOT NULL,
    PRIMARY KEY (id_merk_obat)
);
--
CREATE TABLE OBAT(
    id_obat VARCHAR(10),
    id_produk VARCHAR(10) NOT NULL,
    id_merk_obat VARCHAR(10) NOT NULL,
    netto VARCHAR(10) NOT NULL,
    dosis TEXT NOT NULL,
    aturan_pakai TEXT,
    kontraindikasi TEXT,
    bentuk_kesediaan TEXT NOT NULL,
    PRIMARY KEY (id_obat),
    FOREIGN KEY (id_produk) REFERENCES
    PRODUK (id_produk),
    FOREIGN KEY (id_merk_obat) REFERENCES
    MERK_OBAT (id_merk_obat)
);
--
CREATE TABLE KATEGORI_OBAT(
    id_kategori VARCHAR(10),
    nama_kategori VARCHAR(50) NOT NULL UNIQUE,
    deskripsi_kategori TEXT,
    PRIMARY KEY (id_kategori)
);
--
CREATE TABLE KATEGORI_MERK_OBAT(
    id_kategori VARCHAR(10),
    id_merk_obat VARCHAR(10),
    PRIMARY KEY (id_kategori, id_merk_obat),
    FOREIGN KEY (id_kategori) REFERENCES
    KATEGORI_OBAT (id_kategori),
    FOREIGN KEY (id_merk_obat) REFERENCES
    MERK_OBAT (id_merk_obat)
);
--
CREATE TABLE KOMPOSISI(
    id_bahan VARCHAR(10),
    nama_bahan VARCHAR(50) NOT NULL UNIQUE,
    PRIMARY KEY (id_bahan)
);
--
CREATE TABLE KANDUNGAN(
    id_bahan VARCHAR(10),
    id_obat VARCHAR(10),
    takaran VARCHAR(10),
    satuan_takar VARCHAR(10),
    PRIMARY KEY (id_bahan, id_obat, takaran),
    FOREIGN KEY (id_bahan) REFERENCES
    KOMPOSISI (id_bahan),
    FOREIGN KEY (id_obat) REFERENCES
    OBAT (id_obat)
);
--
CREATE TABLE BALAI_PENGOBATAN(
    id_balai VARCHAR(10),
    alamat_balai TEXT NOT NULL UNIQUE,
    nama_balai VARCHAR(50) NOT NULL,
    jenis_balai VARCHAR(30) NOT NULL,
    telepon_balai VARCHAR(20),
    PRIMARY KEY (id_balai)
);
--
CREATE TABLE BALAI_APOTEK(
    id_balai VARCHAR(10),
    id_apotek VARCHAR(10),
    PRIMARY KEY (id_balai, id_apotek),
    FOREIGN KEY (id_balai) REFERENCES
    BALAI_PENGOBATAN (id_balai),
    FOREIGN KEY (id_apotek) REFERENCES
    APOTEK (id_apotek)
);
--
CREATE TABLE PENGANTARAN_FARMASI(
    id_pengantaran VARCHAR(10),
    id_kurir VARCHAR(10) NOT NULL,
    id_transaksi_pembelian VARCHAR(10),
    waktu TIMESTAMP NOT NULL,
    status_pengantaran VARCHAR(20) NOT NULL,
    biaya_kirim INTEGER NOT NULL,
    total_biaya INTEGER NOT NULL,
    PRIMARY KEY (id_pengantaran),
    FOREIGN KEY (id_kurir) REFERENCES
    KURIR (id_kurir),
    FOREIGN KEY (id_transaksi_pembelian)
    REFERENCES TRANSAKSI_PEMBELIAN (id_transaksi_pembelian)
);
--
INSERT INTO PENGGUNA values('a@pk.com', null, '$pbkdf2-sha256$200000$4XzPOQcgpDSmtNb6v/e.1w$7jhmHjgigeokwYsRdaUXqDXTBrCipbEwIAOD5tVkNNo', 'A');
--
INSERT INTO PENGGUNA values('b@pk.com', null, '$pbkdf2-sha256$200000$IMTYe28NYcz5f69V6j2nVA$74iW5e4mJ4ZXugO7PavMRK.8Nr5EV43MdHIvSbWV17o', 'B');
--
INSERT INTO PENGGUNA values('c@pk.com', null, '$pbkdf2-sha256$200000$7B2jtJZSaq3VGqMU4nyP8Q$gZm4ac6W.DNFpzpxOXbfSvsgwT4kK.WDEa5O/TqDWiI', 'C');
--
INSERT INTO PENGGUNA values('d@pk.com', null, '$pbkdf2-sha256$200000$slbqfU9pLQVAaO0do/T./w$agkFPEDH0np3/GzVOdczrXG0CK4sqDr5kmqhqQw/lGk', 'D');
--
INSERT INTO KONSUMEN values('FKC0000002','a@pk.com', 'L', '1973-10-09');
--
insert into APOTEK values ('FKA0000001', 'pharmacyleaf@fk.com', 'SIA410601758289', 'Bogor', 'PharmacyLeaf', 'Jl Muara Karang Bl B-9 Slt 132-133, Dki Jakarta', 86688243416);
--
insert into apotek values ('FKA0000002', 'pharmaparlor@fk.com', 'SIA205217338890', 'Cilebut', 'PharmaParlor', 'Jl Menanggal Utr WP-02, Jawa Timur', 86128084782);
--
insert into apotek values ('FKA0000003', 'pharmacy@fk.com', 'SIA049050432137', 'Citayam', 'Pharmacy.ly', 'JL. Pejagalan I Kav 72-73, Tambora', 84543163605);
--
insert into apotek values ('FKA0000005', 'pharmacyco@fk.com', 'SIA164459310812', 'Nambo', 'PharmacyCollective', 'Jl Suryani 46, Jawa Barat', 87261089116);
--
INSERT INTO APOTEKER values('b@pk.com');
--
INSERT INTO ADMIN_APOTEK values('b@pk.com','FKA0000001');
--
INSERT INTO APOTEKER values('c@pk.com');
--
insert into CS values ('31290159741065730119', 'c@pk.com', 'SIA003638553952');
--
INSERT INTO KURIR values('FKK0000009','d@pk.com','BB');
--
insert into produk values ('FPK0000001');
--
insert into produk values ('FPK0000002');
--
insert into produk_apotek values (25000, 100, 'btl', 'FPK0000001', 'FKA0000001');
--
insert into produk_apotek values (30000, 88, 'cap', 'FPK0000002', 'FKA0000002');
--
insert into produk values ('FPK0000003');
--
insert into produk values ('FPK0000005');
--
insert into transaksi_pembelian values (1, '2018-02-10 21:17:02.000000', 54000, 'FKC0000002');
--
insert into list_produk_dibeli values (3, 'FKA0000001', 'FPK0000001', 1);






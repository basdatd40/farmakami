from django.db import connection
from django.shortcuts import render, redirect
from merah.views import user_login_required, cursor_fetchall
from .forms import *


# Create your views here.
def index(request):
    context = {}
    return render(request, "kuning.html", context)

@user_login_required
def updatepengantaranfarmasi(request, id_pengantaran):
    if request.session.get("role") != "ADMIN_APOTEK":
        return redirect('/kuning/pengantaranfarmasi')

    select_data = "SELECT * from pengantaran_farmasi" \
                  " WHERE id_pengantaran = '" + id_pengantaran + "';"

    with connection.cursor() as cursor:
        cursor.execute(select_data)
        query_data = cursor.fetchone()
    cursor.close()

    date_db = query_data[3]
    formatted_date = date_db.strftime("%Y-%m-%d %H:%M:%S")

    pengantaran_set = {}
    print(query_data)
    pengantaran_set['id_pengantaran'] = query_data[0]
    pengantaran_set['id_kurir'] = query_data[1]
    pengantaran_set['id_transaksi'] = query_data[2]
    pengantaran_set['waktu'] = formatted_date
    pengantaran_set['status'] = query_data[4]
    pengantaran_set['biaya_kirim'] = query_data[5]
    pengantaran_set['total_biaya'] = query_data[6]

    if request.method == 'POST':
        input_waktu = request.POST.get('input_waktu')
        input_status = request.POST.get('input_status')
        input_biaya_kirim = request.POST.get('input_biaya_kirim')
        with connection.cursor() as cursor:
            try:
                cursor.execute("""
            UPDATE pengantaran_farmasi set waktu=%s, status_pengantaran=%s, biaya_kirim=%s WHERE id_pengantaran=%s
            """, (input_waktu, input_status, input_biaya_kirim, id_pengantaran))
            except IntegrityError:
                pass
            cursor.close()
        return redirect('/kuning/pengantaranfarmasi')
    context = {'pengantaran_set': pengantaran_set}
    return render(request, "UPDATE-PengantaranFarmasi.html", context)


@user_login_required
def updatelistprodukdibeli(request, id_produk, id_apotek, id_transaksi_pembelian):
    select_data = "SELECT * from list_produk_dibeli" \
                  " WHERE id_produk = '" + id_produk + "' AND id_apotek ='" + id_apotek + "'"

    with connection.cursor() as cursor:
        cursor.execute(select_data)
        query_data = cursor.fetchone()
    cursor.close()

    list_jual_set = {}
    print(query_data)
    list_jual_set['jumlah'] = query_data[0]
    list_jual_set['id_apotek'] = query_data[1]
    list_jual_set['id_produk'] = query_data[2]
    list_jual_set['id_transaksi_pembelian'] = query_data[3]

    if request.method == 'POST':
        input_jumlah = request.POST.get('input_jumlah')
        input_id_transaksi = request.POST.get('input_id_transaksi_pembelian')
        with connection.cursor() as cursor:
            try:
                cursor.execute("""
                UPDATE list_produk_dibeli set jumlah=%s,id_apotek=%s,id_produk=%s, id_transaksi_pembelian =%s 
                WHERE id_apotek=%s AND id_produk=%s AND id_transaksi_pembelian=%s
                """, (
                    input_jumlah, id_apotek, id_produk, input_id_transaksi, id_apotek,
                    id_produk, id_transaksi_pembelian))
            except IntegrityError:
                pass
        cursor.close()
        return redirect('/kuning/listprodukdibeli')
    context = {'list_jual_set': list_jual_set}
    return render(request, "UPDATE-ListProdukDibeli.html", context)


@user_login_required
def createlistprodukdibeli(request):
    if request.session.get("role") != "ADMIN_APOTEK":
        return redirect('/kuning/listprodukdibeli')
    if request.method == 'POST':
        form = ProductDibeliForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("/kuning/listprodukdibeli")
    elif request.method == 'GET':
        form = ProductDibeliForm()
    context = {'form': form}
    return render(request, "CREATE-ListProdukDibeli.html", context)


@user_login_required
def createpengantaranfarmasi(request):
    if request.session.get("role") != "ADMIN_APOTEK":
        return redirect('/kuning/pengantaranfarmasi')
    if request.method == 'POST':
        form = PengantaranFarmasiForms(request.POST)
        if form.is_valid():
            form.save()
            return redirect("/kuning/pengantaranfarmasi")
    elif request.method == 'GET':
        form = PengantaranFarmasiForms()
    context = {'form': form}
    return render(request, "CREATE-PengantaranFarmasi.html", context)


def pengantaranfarmasi(request):
    with connection.cursor() as cursor:
        cursor.execute("SELECT * FROM pengantaran_farmasi ORDER BY id_pengantaran")
        data_produk_dibeli = cursor_fetchall(cursor)
    context = {'data': data_produk_dibeli}
    return render(request, "READ-PengantaranFarmasi.html", context)


@user_login_required
def listprodukdibeli(request):
    with connection.cursor() as cursor:
        cursor.execute("SELECT * FROM LIST_PRODUK_DIBELI ORDER BY id_produk")
        data_produk_dibeli = cursor_fetchall(cursor)

    context = {'data': data_produk_dibeli}
    return render(request, "READ-ListProdukDibeli.html", context)


@user_login_required
def delete_list_produk_dibeli(request):
    if request.session.get("role") != "ADMIN_APOTEK":
        return redirect('/kuning/listprodukdibeli')
    if request.method == 'GET':
        if None in [request.GET.get('delete-apotik-dibeli'), request.GET.get('delete-produk-dibeli')]:
            return redirect('/kuning/listprodukdibeli')
        else:
            try:
                with connection.cursor() as cursor:
                    with transaction.atomic():
                        cursor.execute("""\
                            DELETE FROM list_produk_dibeli where id_apotek =%s\
                            and id_produk=%s
                            """, [request.GET.get('delete-apotik-dibeli'),
                                  request.GET.get('delete-produk-dibeli')])
            except IntegrityError:
                pass
        return redirect('/kuning/listprodukdibeli')


@user_login_required
def delete_pengantaran_farmasi(request):
    if request.session.get("role") != "ADMIN_APOTEK":
        return redirect('/kuning/pengantaranfarmasi')
        return redirect('/kuning/pengantaranfarmasi')
    if request.method == 'GET':
        if request.GET.get('delete_pengantaran') is None:
            return redirect('/kuning/pengantaranfarmasi')
        else:
            id = request.GET.get('delete_pengantaran')
            try:
                with connection.cursor() as cursor:
                    with transaction.atomic():
                        cursor.execute("""\
                        DELETE FROM pengantaran_farmasi where id_pengantaran =%s\
                        """, [id])
            except IntegrityError:
                pass
    return redirect('/kuning/pengantaranfarmasi')

from django.urls import path

urlpatterns = []
from kuning.views import *

urlpatterns = [
    path('', index, name="index"),
    path('listprodukdibeli', listprodukdibeli, name="listprodukdibeli"),
    path('update-listprodukdibeli/<str:id_produk>/<str:id_apotek>/<str:id_transaksi_pembelian>', updatelistprodukdibeli,
         name="update-listprodukdibeli"),
    path('create-listprodukdibeli', createlistprodukdibeli, name="create-listprodukdibeli"),
    path('create-pengantaranfarmasi', createpengantaranfarmasi, name="create-pengantaranfarmasi"),
    path('pengantaranfarmasi', pengantaranfarmasi, name="pengantaranfarmasi"),
    path('update-pengantaranfarmasi/<str:id_pengantaran>', updatepengantaranfarmasi, name="update-pengantaranfarmasi"),
    path('delete-produkdibeli', delete_list_produk_dibeli, name='delete-produkdibeli'),
    path('delete-pengantaranfarmasi', delete_pengantaran_farmasi, name='delete-pengantaranfarmasi'),
]

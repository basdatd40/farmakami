from datetime import datetime
from django import forms
from django.db import connection, transaction, IntegrityError, InternalError


class ProductDibeliForm(forms.Form):
    PRODUK_CHOICES = []
    APOTEK_CHOICES = []
    TRANSAKSI_CHOICES = []

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        with connection.cursor() as cursor:
            cursor.execute("SELECT id_produk, id_produk FROM PRODUK")
            self.fields['id_produk'].choices = cursor.fetchall()
            cursor.execute("SELECT id_apotek, id_apotek FROM APOTEK")
            self.fields['id_apotek'].choices = cursor.fetchall()
            cursor.execute("SELECT id_transaksi_pembelian, id_transaksi_pembelian FROM transaksi_pembelian ")
            self.fields['id_transaksi'].choices = cursor.fetchall()

    id_produk = forms.ChoiceField(choices=PRODUK_CHOICES,
                                  widget=forms.Select(attrs={'class': 'form-control'}))
    id_apotek = forms.ChoiceField(choices=APOTEK_CHOICES,
                                  widget=forms.Select(attrs={'class': 'form-control'}))
    id_transaksi = forms.ChoiceField(choices=TRANSAKSI_CHOICES,
                                     widget=forms.Select(attrs={'class': 'form-control'}))
    jumlah = forms.IntegerField(
        widget=forms.NumberInput(attrs={'class': 'form-control'}))

    def save(self):
        cd = self.cleaned_data
        try:
            with connection.cursor() as cursor:
                cursor.execute("INSERT INTO list_produk_dibeli VALUES(%s, %s, %s, %s)",
                               [cd.get("jumlah"), cd.get("id_apotek"),
                                cd.get("id_produk"), cd.get("id_transaksi")])
        except IntegrityError:
            pass

    def clean(self):
        id_produk = self.cleaned_data.get("id_produk")
        id_apotek = self.cleaned_data.get("id_apotek")
        with connection.cursor() as cursor:
            cursor.execute("SELECT id_produk, id_apotek FROM PRODUK_APOTEK")
            existing_data = cursor.fetchall()
        if not (id_produk, id_apotek) in existing_data:
            raise forms.ValidationError("Masukan data gagal karena produk dibeli tidak terdaftar di apotik")


class PengantaranFarmasiForms(forms.Form):
    TRANSAKSI_CHOICES = []

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        with connection.cursor() as cursor:
            cursor.execute("SELECT id_transaksi_pembelian, id_transaksi_pembelian FROM transaksi_pembelian ")
            self.fields['id_transaksi'].choices = cursor.fetchall()

    id_transaksi = forms.ChoiceField(choices=TRANSAKSI_CHOICES,
                                     widget=forms.Select(attrs={'class': 'form-control'}))
    waktu = forms.DateTimeField(initial=datetime.today())
    biaya = forms.IntegerField(
        widget=forms.NumberInput(attrs={'class': 'form-control'}))

    def save(self):
        id_kurir_default = "FKK0000001"
        status_pengantaran_default = "IN TRANSIT"
        cd = self.cleaned_data

        with connection.cursor() as cursor:
            cursor.execute(
                "SELECT MAX(CAST(SUBSTRING(id_pengantaran,4,LENGTH(id_pengantaran)-3) AS INT)) FROM pengantaran_farmasi WHERE id_pengantaran LIKE 'IDK%' ")
            records = cursor.fetchall()
            for i in records:
                pengantaran = i[0] + 1

        if pengantaran < 10:
            insert_pengantaran = "IDK000000" + str(pengantaran)
        elif pengantaran < 100:
            insert_pengantaran = "IDK00000" + str(pengantaran)
        elif pengantaran < 1000:
            insert_pengantaran = "IDK0000" + str(pengantaran)
        else:
            insert_pengantaran = "IDK0000000"

        with connection.cursor() as cursor:
            cursor.execute("INSERT INTO pengantaran_farmasi VALUES(%s, %s, %s, %s,%s,%s,%s)",
                           [insert_pengantaran, id_kurir_default, cd.get('id_transaksi'),
                            cd.get('waktu'), status_pengantaran_default, cd.get('biaya'), 0])


class UpdateListProduk(ProductDibeliForm):
    curr_id_produk = forms.CharField(
        widget=forms.HiddenInput(attrs={'class': 'form-control',
                                        'readonly': True}),
        max_length=10,
    )
    curr_id_apotek = forms.CharField(
        widget=forms.HiddenInput(attrs={'class': 'form-control',
                                        'readonly': True}),
        max_length=10,
    )

    def save_update(self):
        cd = self.cleaned_data
        try:
            with connection.cursor() as cursor:
                with transaction.atomic():
                    cursor.execute("""
                    UPDATE LIST_PRODUK_DIBELI SET jumlah = %s, id_apotek = %s,
                    id_produk = %s, id_transaksi_pembelian = %s
                    WHERE id_produk=%s AND id_apotek=%s""",
                                   [cd.get("jumlah"), cd.get("id_apotek"),
                                    cd.get("id_produk"), cd.get("id_transaksi"),
                                    cd.get("curr_id_produk"), cd.get("curr_id_apotek")])
        except IntegrityError:
            pass


class UpdatePengantaranFarmasi(PengantaranFarmasiForms):
    id_pengantaran = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}),
                                     max_length=50
                                     )

    id_kurir = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}),
                               max_length=50
                               )
    status = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control'}),
        max_length=50
    )

    total_biaya = forms.IntegerField(widget=forms.NumberInput(attrs={'class': 'form-control',
                                                                     'readonly': 'True'}))

    def save(self):
        cd = self.cleaned_data
        try:
            with connection.cursor() as cursor:
                cursor.execute(
                    """
                    UPDATE PENGANTARAN_FARMASI SET id_pengantaran=%s,id_kurir=%s,id_transaksi_pembelian=%s,waktu=%s,
                    status_pengantaran=%s, biaya_kirim=%s,total_biaya=%s
                    WHERE id_pengantaran=%s
                    """,
                    [cd.get('id_pengantaran'), cd.get("id_kurir"), cd.get('id_transaksi'), cd.get('waktu'),
                     cd.get('status'), cd.get('biaya'), cd.get('total_biaya'), cd.get('id_pengantaran')]
                )
        except InternalError:
            with connection.cursor() as cursor:
                cursor.execute(
                    """
                    UPDATE PENGANTARAN_FARMASI SET id_pengantaran=%s,id_kurir=%s,id_transaksi_pembelian=%s,waktu=%s,
                    status_pengantaran=%s, biaya_kirim=%s,total_biaya=%s
                    WHERE id_pengantaran=%s
                    """,
                    [cd.get('id_pengantaran'), cd.get("id_kurir"), cd.get('id_transaksi'), cd.get('waktu'),
                     'PROCESSING', cd.get('biaya'), cd.get('total_biaya'), cd.get('id_pengantaran')]
                )

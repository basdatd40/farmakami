from django.urls import path, include
from .views import *
app_name = 'biru'
urlpatterns = [ 

	path('balai_pengobatan/', balai_pengobatan, name='balai_pengobatan'), 
	path('create_balai_pengobatan/', create_balai_pengobatan, name='create_balai_pengobatan'),
	path('update_balai_pengobatan/', update_balai_pengobatan, name='update_balai_pengobatan'),
	path('delete_balai_pengobatan/', delete_balai_pengobatan, name='delete_balai_pengobatan'),   
 
	path('obat/', obat, name='obat'), 
	path('create_obat/', create_obat, name='create_obat'),
	path('update_obat/', update_obat, name='update_obat'),
	path('delete_obat/', delete_obat, name='delete_obat'),

]
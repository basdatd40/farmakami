from django.shortcuts import render, redirect, get_object_or_404
from django.db import connection, transaction, IntegrityError
from .forms import *
from merah.views import user_login_required, cursor_fetchall


# Create your views here.
def balai_pengobatan(request):
	with connection.cursor() as cursor:
		# cursor.execute("SELECT * FROM BALAI_PENGOBATAN ORDER BY id_balai")
		cursor.execute(
			"""
			SELECT BP.id_balai, alamat_balai, nama_balai, jenis_balai, telepon_balai, nama_apotek
			FROM BALAI_PENGOBATAN as BP
			LEFT JOIN BALAI_APOTEK as BA
			ON BA.id_balai = BP.id_balai
			LEFT JOIN APOTEK as A
			ON BA.id_apotek = A.id_apotek
			ORDER BY BP.id_balai""")
		data_balai_pengobatan = cursor_fetchall(cursor)
	context = {'data': data_balai_pengobatan}
	return render(request, "BalaiPengobatan.html", context)
	

@user_login_required
def create_balai_pengobatan(request):

	if request.session.get("role") != "ADMIN_APOTEK":
		return redirect('biru:balai_pengobatan')
	if request.method == 'POST':
		form = Create_Balai_Pengobatan_Form(request.POST)
		if form.is_valid():
			form.save()
			return redirect("biru:balai_pengobatan")
	if request.method == 'GET':
		form = Create_Balai_Pengobatan_Form()
	context = {'form': form}
	return render(request, "CreateBalaiPengobatan.html", context)


@user_login_required
def update_balai_pengobatan(request):

	if request.session.get("role") != "ADMIN_APOTEK":
		return redirect('biru:balai_pengobatan')
	if request.method == 'POST':
		form = Update_Balai_Pengobatan_Form(request.POST)
		if form.is_valid():
			form.save()
			return redirect("biru:balai_pengobatan")
	elif request.method == 'GET':
		if request.GET.get('id_u_b') is None:
			return redirect('biru:balai_pengobatan')
		with connection.cursor() as cursor:
			cursor.execute("SELECT * FROM BALAI_PENGOBATAN where id_balai=%s LIMIT 1;",
							[request.GET.get('id_u_b')])
			data = cursor.fetchone()
			if data is None:
				return redirect('biru:balai_pengobatan')
			columns = [col[0] for col in cursor.description]
			data_balai_pengobatan = dict(zip(columns, data))

		form = Update_Balai_Pengobatan_Form(initial=data_balai_pengobatan)

	context = {'form': form}
	return render(request, "UpdateBalaiPengobatan.html", context)

@user_login_required
def delete_balai_pengobatan(request):
	if request.session.get("role") != "ADMIN_APOTEK":
		return redirect('biru:balai_pengobatan')
	if request.method == 'GET':
		if request.GET.get('id_d_b') is None:
			return redirect('biru:balai_pengobatan')
		else:
			id = request.GET.get('id_d_b')
			try:
				with connection.cursor() as cursor:
					with transaction.atomic():
						cursor.execute("""
						DELETE FROM BALAI_APOTEK WHERE id_balai=%s
						""", [id])
						cursor.execute("""
						DELETE FROM BALAI_PENGOBATAN WHERE id_balai=%s
						""", [id])
			except IntegrityError:
				pass
			return redirect('biru:balai_pengobatan')

def obat(request):
	with connection.cursor() as cursor:
		cursor.execute("SELECT * FROM OBAT ORDER BY id_obat")
		data_obat = cursor_fetchall(cursor)

	context = {'data': data_obat}
	return render(request, "Obat.html", context)


@user_login_required
def create_obat(request):
	if request.session.get("role") != "ADMIN_APOTEK" and request.session.get("role") != "CS" :
		return redirect('biru:obat')
	if request.method == 'POST':
		form = Create_Obat_Form(request.POST)
		if form.is_valid():
			form.save()
			return redirect("biru:obat")
	if request.method == 'GET':
		form = Create_Obat_Form()
	context = {'form': form}
	return render(request, "CreateObat.html", context)

	
@user_login_required
def update_obat(request):

	if request.session.get("role") != "ADMIN_APOTEK" and request.session.get("role") != "CS" :
		return redirect('biru:obat')
	if request.method == 'POST':
		form = Update_Obat_Form(request.POST)
		if form.is_valid():
			form.save()
			return redirect("biru:obat")
	elif request.method == 'GET':
		if request.GET.get('id_u_o') is None:
			return redirect('biru:obat')
		with connection.cursor() as cursor:
			cursor.execute("SELECT * FROM OBAT where id_obat=%s LIMIT 1;",
							[request.GET.get('id_u_o')])
			data = cursor.fetchone()
			if data is None:
				return redirect('biru:obat')
			columns = [col[0] for col in cursor.description]
			data_obat = dict(zip(columns, data))

		form = Update_Obat_Form(initial=data_obat)

	context = {'form': form}
	return render(request, "UpdateObat.html", context)

@user_login_required
def delete_obat(request):

	if request.session.get("role") != "ADMIN_APOTEK" and request.session.get("role") != "CS" :		
		return redirect('biru:obat')
	if request.method == 'GET':
		if request.GET.get('id_d_o') is None:
			return redirect('biru:obat')
		else:
			id = request.GET.get('id_d_o')
			try:
				with connection.cursor() as cursor:
					with transaction.atomic():
						cursor.execute("""
						DELETE FROM OBAT WHERE id_obat=%s
						""", [id])
			except IntegrityError:
				pass
			return redirect('biru:obat')


from django import forms
from django.forms import Form
from django.db import connection, transaction, IntegrityError

class Balai_Pengobatan_Form(forms.Form):

	APOTEK_CHOICES = []

	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		with connection.cursor() as cursor:
			cursor.execute("SELECT id_apotek, id_apotek FROM APOTEK")
			self.fields['id_apotek'].choices = cursor.fetchall()

	pos_attrs = {
		'class': 'form-control',
		
	}
	alamat_balai = forms.CharField(label="Alamat Balai", widget=forms.TextInput(attrs=pos_attrs), required=True)
	nama_balai = forms.CharField(label='Nama Balai', required=True, max_length=50, widget=forms.TextInput(attrs=pos_attrs))   
	jenis_balai = forms.CharField(label="Jenis Balai", required=True, max_length=30, widget=forms.TextInput(attrs=pos_attrs))
	telepon_balai = forms.CharField(label="Telepon Balai", required=False, max_length=20, widget=forms.NumberInput(attrs=pos_attrs))
	id_apotek = forms.ChoiceField(label='ID Apotek yang Berasosiasi', required=True, choices=APOTEK_CHOICES,
								widget=forms.Select(attrs={'class': 'form-control'}))
  
class Update_Balai_Pengobatan_Form(Balai_Pengobatan_Form):

	id_balai = forms.CharField(label="ID Balai",
		widget=forms.TextInput(attrs={'class': 'form-control',
									  'readonly': True}),
		max_length=10
	)

	def save(self):
		cd = self.cleaned_data
		with connection.cursor() as cursor:
			cursor.execute("""
			UPDATE BALAI_PENGOBATAN SET alamat_balai = %s, nama_balai = %s, jenis_balai = %s,
			telepon_balai = %s
			WHERE id_balai = %s """,
						   [cd.get("alamat_balai"), cd.get("nama_balai"),
							cd.get("jenis_balai"), cd.get("telepon_balai"),
							cd.get("id_balai")])
			cursor.execute("""
			UPDATE BALAI_APOTEK SET id_apotek = %s
			WHERE id_balai = %s""", [cd.get("id_apotek"), cd.get("id_balai")])

			cursor.execute("""SELECT id_balai FROM BALAI_APOTEK""")
			balai_berasosiasi = cursor.fetchall()
			if cd.get("id_balai") not in balai_berasosiasi:
				cursor.execute("INSERT INTO BALAI_APOTEK VALUES(%s, %s)",
						   [cd.get("id_balai"), cd.get("id_apotek")
							])

		


class Create_Balai_Pengobatan_Form(Balai_Pengobatan_Form):
	def clean_alamat_balai(self):
		data = self.cleaned_data.get('alamat_balai')
		with connection.cursor() as cursor:
			cursor.execute("SELECT * FROM BALAI_PENGOBATAN WHERE alamat_balai=%s LIMIT 1",
						   [data])
			row = cursor.fetchone()
		if row is not None:
			raise forms.ValidationError("Alamat balai yang dimasukkan harus unik")
		return data

	def save(self):
		cd = self.cleaned_data
		with connection.cursor() as cursor:
			cursor.execute("SELECT * FROM BALAI_PENGOBATAN ORDER BY id_balai DESC LIMIT 1")
			add_id_balai = int(cursor.fetchone()[0][3:]) + 1
			new_id_balai= f"FBP{add_id_balai:07}"
		
			cursor.execute("INSERT INTO BALAI_PENGOBATAN VALUES(%s, %s, %s, %s, %s)",
						   [new_id_balai, cd.get("alamat_balai"), cd.get("nama_balai"),
							cd.get("jenis_balai"), cd.get("telepon_balai")
							])
			cursor.execute("INSERT INTO BALAI_APOTEK VALUES(%s, %s)",
						   [new_id_balai, cd.get("id_apotek")
							])  
  


class Obat_Form(forms.Form):

	MERK_OBAT_CHOICES = []

	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		with connection.cursor() as cursor:
			cursor.execute("SELECT id_merk_obat, id_merk_obat FROM MERK_OBAT")
			self.fields['id_merk_obat'].choices = cursor.fetchall()


	pos_attrs = {
		'class': 'form-control',

	}
	id_merk_obat = forms.ChoiceField(label='ID Merk Obat', required=True, choices= MERK_OBAT_CHOICES, widget=forms.Select(attrs={'class': 'form-control'}))
	netto = forms.CharField(label="Netto", required=True, max_length=10, widget=forms.TextInput(attrs=pos_attrs))
	dosis = forms.CharField(label="Dosis", required=True, widget=forms.TextInput(attrs=pos_attrs))   
	aturan_pakai = forms.CharField(label="Aturan Pakai", required=False, widget=forms.TextInput(attrs=pos_attrs))
	kontraindikasi = forms.CharField(label="Kontraindikasi", required=False, widget=forms.TextInput(attrs=pos_attrs))
	bentuk_kesediaan = forms.CharField(label="Bentuk Kesediaan", required=True, widget=forms.TextInput(attrs=pos_attrs))


class Update_Obat_Form(Obat_Form):
	id_obat = forms.CharField(label="ID Obat",
		widget=forms.TextInput(attrs={'class': 'form-control',
									  'readonly': True}),
		max_length=10
	)

	id_produk = forms.CharField(label="ID Produk", 
		widget=forms.TextInput(attrs={'class': 'form-control',
									  'readonly': True}),
		max_length=10
	)


	def save(self):
		cd = self.cleaned_data
		with connection.cursor() as cursor:
			cursor.execute("""
			UPDATE OBAT SET id_merk_obat = %s, netto = %s, dosis = %s,
			aturan_pakai = %s, kontraindikasi = %s, bentuk_kesediaan = %s
			WHERE id_obat=%s """,
						   [cd.get("id_merk_obat"), cd.get("netto"),
							cd.get("dosis"), cd.get("aturan_pakai"),
							cd.get("kontraindikasi"), cd.get("bentuk_kesediaan"),
							cd.get("id_obat")])

class Create_Obat_Form(Obat_Form):
	def save(self):
		cd = self.cleaned_data
		with connection.cursor() as cursor:
			cursor.execute("SELECT * FROM OBAT ORDER BY id_obat DESC LIMIT 1")
			add_id_obat = int(cursor.fetchone()[0][3:]) + 1
			new_id_obat = f"FOB{add_id_obat:07}"
			cursor.execute("SELECT * FROM PRODUK ORDER BY id_produk DESC LIMIT 1")
			add_id_produk = int(cursor.fetchone()[0][3:]) + 1
			new_id_produk = f"FPK{add_id_produk:07}"
			cursor.execute("INSERT INTO PRODUK VALUES(%s)",
						   [new_id_produk])
			cursor.execute("INSERT INTO OBAT VALUES(%s, %s, %s, %s, %s, %s, %s, %s)",
						   [new_id_obat, new_id_produk, cd.get("id_merk_obat"), cd.get("netto"),
							cd.get("dosis"), cd.get("aturan_pakai"),
							cd.get("kontraindikasi"), cd.get("bentuk_kesediaan")
							])
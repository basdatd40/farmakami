from django import forms
from django.db import connection, transaction, IntegrityError

class BaseTransaksiDenganResepForm(forms.Form):
    ID_CHOICES  = []

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        with connection.cursor() as cursor:
            cursor.execute("SELECT id_transaksi_pembelian, id_transaksi_pembelian FROM TRANSAKSI_DENGAN_RESEP")
            self.fields['id_transaksi_pembelian'].choices = cursor.fetchall()

    nama_dokter = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control'}),
        max_length=50
    )
    nama_pasien = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control'}),
        max_length=50
    )
    
    isi_resep = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control'}),
        max_length=50
    )

    unggahan_resep = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control'}),
        max_length=50
    )

    tanggal_resep = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control'}),
        max_length=50
    )

    no_telepon = forms.CharField(
        widget=forms.NumberInput(attrs={
            'class': 'form-control',
            'placeholder': '0xx (w/o country code)'}),
        max_length=20
    )

    id_transaksi_pembelian = forms.ChoiceField(choices= ID_CHOICES,
                                  widget=forms.Select(attrs={'class': 'form-control'})
    )

    def clean_email(self):
        data = self.cleaned_data.get('email_apoteker')
        with connection.cursor() as cursor:
            cursor.execute("SELECT email FROM APOTEKER WHERE email=%s",
                           [data])
            if cursor.fetchone() is None:
                raise forms.ValidationError("Email Apoteker tidak ada")
        return data




class TransaksiDenganResepForm(BaseTransaksiDenganResepForm):
    EMAIL_CHOICES  = []

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        with connection.cursor() as cursor:
            cursor.execute("SELECT email, email FROM APOTEKER")
            self.fields['email_apoteker'].choices = cursor.fetchall()

    no_resep = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control',
                                      'readonly': True}),
        max_length=13
    )

    status_validasi = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control'}),
        max_length=50
    )

    email_apoteker = forms.ChoiceField(choices= EMAIL_CHOICES,
                                  widget=forms.Select(attrs={'class': 'form-control'})
    )


    def save(self):
        cd = self.cleaned_data
        with connection.cursor() as cursor:
            cursor.execute("""
            UPDATE TRANSAKSI_DENGAN_RESEP SET nama_dokter = %s, nama_pasien = %s, isi_resep = %s,
            unggahan_resep = %s, tanggal_resep = %s, status_validasi = %s , no_telepon = %s ,
            id_transaksi_pembelian = %s, email_apoteker = %s
            WHERE no_resep=%s
                           """,
                           [cd.get("nama_dokter"), cd.get("nama_pasien"),
                            cd.get("isi_resep"), cd.get("unggahan_resep"),
                            cd.get("tanggal_resep"), cd.get("status_validasi"),
                            cd.get("no_telepon"), cd.get("id_transaksi_pembelian"),
                            cd.get("email_apoteker"), cd.get("no_resep"),])
    

class NewTransaksiResepForm(BaseTransaksiDenganResepForm):
    def get_new_no_resep(self):
        cursor = connection.cursor()
        cursor.execute("SELECT no_resep FROM TRANSAKSI_DENGAN_RESEP")
        columns = [col[0] for col in cursor.description]
        data_id = [dict(zip(columns,row)) for row in cursor.fetchall()]
        
        packed_data = []
        for data in data_id:
            number = int(data['no_resep'][-10:])
            packed_data.append(number)  
        selected = packed_data[0]
        while(selected in packed_data):
            selected += 1
        return f"RES{selected}"

    def save(self):
        cd = self.cleaned_data
        with connection.cursor() as cursor:
            cursor.execute("SELECT * FROM TRANSAKSI_DENGAN_RESEP ORDER BY id_transaksi_pembelian DESC LIMIT 1")
            new_no_resep = str(self.get_new_no_resep())
            status = "INVALID"
            cursor.execute("INSERT INTO TRANSAKSI_DENGAN_RESEP VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
                           [new_no_resep, cd.get("nama_dokter"), cd.get("nama_pasien"),
                            cd.get("isi_resep"), cd.get("unggahan_resep"),
                            cd.get("tanggal_resep"), status, cd.get("no_telepon"), cd.get("id_transaksi_pembelian"),
                            cd.get("email_apoteker")
                            ])


class BaseAlatMedisForm(forms.Form):
    nama_alat_medis = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control'}),
        max_length=50
    )
    deskripsi_alat = forms.CharField(
        widget=forms.Textarea(attrs={'class': 'form-control',
                                     'rows': 3}),
    )
    
    jenis_penggunaan = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control'}),
        max_length=50
    )

class AlatMedisForm(BaseAlatMedisForm):
    id_alat_medis = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control',
                                      'readonly': True}),
        max_length=13
    )
    

    id_produk = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control',
                                      'readonly': True}),
        max_length=13
    )

    def save(self):
        cd = self.cleaned_data
        with connection.cursor() as cursor:
            cursor.execute("""
            UPDATE ALAT_MEDIS SET id_alat_medis = %s, nama_alat_medis = %s, deskripsi_alat = %s,
            jenis_penggunaan = %s, id_produk = %s
            WHERE id_alat_medis=%s
                           """,
                           [cd.get("id_alat_medis"), cd.get("nama_alat_medis"),
                            cd.get("deskripsi_alat"), cd.get("jenis_penggunaan"),
                            cd.get("id_produk"),cd.get("id_alat_medis"),])
    
class NewAlatMedisForm(BaseAlatMedisForm):
    def get_new_id_alat_medis(self):
        cursor = connection.cursor()
        cursor.execute("SELECT id_alat_medis FROM ALAT_MEDIS")
        columns = [col[0] for col in cursor.description]
        data_id = [dict(zip(columns,row)) for row in cursor.fetchall()]
        
        packed_data = []
        for data in data_id:
            number = int(data['id_alat_medis'][-7:])
            packed_data.append(number)  
        selected = packed_data[0]
        while(selected in packed_data):
            selected += 1
        return f"FAM00000{selected}"

    def save(self):
        cd = self.cleaned_data
        with connection.cursor() as cursor:
            new_id_alat_medis =  str(self.get_new_id_alat_medis())
            id_produk = "FPK0000003"
            cursor.execute("""
            INSERT INTO ALAT_MEDIS VALUES(%s, %s, %s, %s, %s) \
                """,
                [new_id_alat_medis, cd.get("nama_alat_medis"),
                cd.get("deskripsi_alat"), cd.get("jenis_penggunaan"),
                id_produk,])


from django.shortcuts import render, redirect
from django.db import connection, transaction, IntegrityError
from .forms import *


def user_login_required(function):
    def wrapper(request, *args, **kwargs):
        email = request.session.get('email')
        if email is None:
            return redirect('/login/')
        else:
            return function(request, *args, **kwargs)
    return wrapper


def cursor_fetchall(cursor):
    columns = [col[0] for col in cursor.description]
    return [dict(zip(columns, row)) for row in cursor.fetchall()]


def transaksiresep(request):
  with connection.cursor() as cursor:
      cursor.execute("SELECT * FROM TRANSAKSI_DENGAN_RESEP ORDER BY id_transaksi_pembelian")
      data_transaksi = cursor_fetchall(cursor)

  context = {'data': data_transaksi}
  return render(request,"READ-TransaksiResep.html",context)

@user_login_required
def deletetransaksiresep(request):
    if request.session.get("role") != "ADMIN_APOTEK":
        return redirect('/ungu/transaksiresep')
    if request.method == 'GET':
        if request.GET.get('id_d') is None:
            return redirect('/ungu/transaksiresep')
        else:
            id = request.GET.get('id_d')
            try:
                with connection.cursor() as cursor:
                    with transaction.atomic():
                        cursor.execute("""\
                        DELETE FROM TRANSAKSI_DENGAN_RESEP WHERE no_resep=%s\
                        """, [id])
            except IntegrityError:
                pass
            return redirect('/ungu/transaksiresep')

@user_login_required
def updatetransaksiresep(request):
  if request.session.get("role") != "ADMIN_APOTEK":
    return redirect('/ungu/transaksiresep')
  if request.method == 'POST':
      form = TransaksiDenganResepForm(request.POST)
      if form.is_valid():
          form.save()
          return redirect("/ungu/transaksiresep")  
  elif request.method == 'GET':
    if request.GET.get('id_u') is None:
        return redirect('/ungu/transaksiresep')
    with connection.cursor() as cursor:
        cursor.execute("SELECT * FROM TRANSAKSI_DENGAN_RESEP WHERE no_resep=%s LIMIT 1;",
                        [request.GET.get('id_u')])
        data = cursor.fetchone()
        if data is None:
            return redirect('/ungu/transaksiresep')
        columns = [col[0] for col in cursor.description]
        data_transaksi = dict(zip(columns, data))
    form = TransaksiDenganResepForm(initial=data_transaksi)
  
  context = {'form': form}
  return render(request,"UPDATE-TransaksiResep.html",context)

@user_login_required
def createtransaksiresep(request):
  if request.session.get("role") != "ADMIN_APOTEK":
    return redirect('/ungu/transaksiresep')
  if request.method == 'POST':
    form = NewTransaksiResepForm(request.POST)
    if form.is_valid():
        form.save()
        return redirect("/ungu/transaksiresep")
  if request.method == 'GET':
      form = NewTransaksiResepForm()
  context = {'form': form}
  return render(request,"CREATE-TransaksiResep.html",context)




def alatmedis(request):
  with connection.cursor() as cursor:
      cursor.execute("SELECT * FROM alat_medis ORDER BY id_alat_medis")
      data_transaksi = cursor_fetchall(cursor)

  context = {'data': data_transaksi}
  return render(request,"READ-AlatMedis.html",context)

@user_login_required
def deletealatmedis(request):
    if request.session.get("role") != "ADMIN_APOTEK":
        return redirect('/ungu/alatmedis')
    if request.method == 'GET':
        if request.GET.get('id_d') is None:
            return redirect('/ungu/alatmedis')
        else:
            id = str(request.GET.get('id_d'))
            try:
                with connection.cursor() as cursor:
                    with transaction.atomic():
                        cursor.execute("""\
                        DELETE FROM ALAT_MEDIS WHERE id_alat_medis=%s\
                        """, [id])
            except IntegrityError:
                pass
            return redirect('/ungu/alatmedis')

@user_login_required
def updatealatmedis(request):
    if request.session.get("role") != "ADMIN_APOTEK":
        return redirect('/ungu/alatmedis')
    if request.method == 'POST':
        form = AlatMedisForm(request.POST)
        if form.is_valid():
            print("cicak")
            form.save()
            return redirect("/ungu/alatmedis")  
    elif request.method == 'GET':
        if request.GET.get('id_d') is None:
            return redirect('/ungu/alatmedis')
        with connection.cursor() as cursor:
            cursor.execute("SELECT * FROM ALAT_MEDIS WHERE id_alat_medis=%s LIMIT 1;",
                            [request.GET.get('id_d')])
            data = cursor.fetchone()
            if data is None:
                return redirect('/ungu/alatmedis')
            columns = [col[0] for col in cursor.description]
            data_transaksi = dict(zip(columns, data))
        form = AlatMedisForm(initial=data_transaksi)    
    context = {'form': form}
    return render(request,"UPDATE-AlatMedis.html",context)

@user_login_required
def createalatmedis(request):
    if request.session.get("role") != "ADMIN_APOTEK":
        return redirect('/ungu/alatmedis')
    if request.method == 'POST':
        form = NewAlatMedisForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("/ungu/alatmedis")
    if request.method == 'GET':
        form = NewAlatMedisForm()
    context = {'form': form}
    return render(request,"CREATE-AlatMedis.html",context)

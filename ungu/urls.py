from django.urls import path
from ungu.views import *

urlpatterns = [
    path('transaksiresep', transaksiresep,name="transaksiresep"),
    path('update-transaksiresep', updatetransaksiresep,name="update-transaksiresep"),
    path('create-transaksiresep', createtransaksiresep,name="create-transaksiresep"),
    path('delete-transaksiresep', deletetransaksiresep,name="delete-transaksiresep"),
    path('alatmedis',alatmedis,name="alatmedis"),
    path('update-alatmedis', updatealatmedis,name="update-alatmedis"),
    path('create-alatmedis', createalatmedis,name="create-alatmedis"),
    path('delete-alatmedis', deletealatmedis,name="delete-alatmedis"),
]
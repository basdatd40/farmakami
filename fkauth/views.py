from django.db import connection
from django.shortcuts import render, redirect

from fkauth.forms import *
from merah.views import user_login_required


def home(request):
    return render(request, "index.html")


def no_user(function):
    def wrapper(request, *args, **kwargs):
        email = request.session.get('email')
        if email is not None:
            return redirect('/profile/')
        else:
            return function(request, *args, **kwargs)
    return wrapper


def check_role(email):
    with connection.cursor() as cursor:
        for i in ["KONSUMEN", "ADMIN_APOTEK", "CS", "KURIR"]:
            cursor.execute(
                f"SELECT * FROM {i} WHERE email=%s LIMIT 1;",
                [email])
            if cursor.fetchone() is not None:
                return i


def get_role_data(email, role):
    with connection.cursor() as cursor:
        cursor.execute(
            f"SELECT * FROM {role} WHERE email=%s LIMIT 1;",
            [email])
        return cursor.fetchone()


def get_addresses(id_konsumen):
    with connection.cursor() as cursor:
        cursor.execute(
            "SELECT * FROM alamat_konsumen WHERE id_konsumen=%s;",
            [id_konsumen])
        columns = [col[0] for col in cursor.description]
        data_alamat = [
            dict(zip(columns, row))
            for row in cursor.fetchall()
        ]
        return data_alamat


def input_session(request, email, nama):
    request.session['email'] = email
    request.session['nama'] = nama
    request.session['role'] = check_role(email)


@no_user
def login(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            with connection.cursor() as cursor:
                cursor.execute(
                    "SELECT * FROM PENGGUNA WHERE email=%s LIMIT 1;",
                    [form.cleaned_data.get('email')])
                data = cursor.fetchone()
            input_session(request, form.cleaned_data.get('email'), data[3])
            return redirect('/profile/')
    elif request.method == 'GET':
        form = LoginForm()
    context = {'form': form}
    return render(request, "login.html", context)


def logout(request):
    request.session.flush()
    return redirect('/')


@no_user
def register(request):
    if request.method == 'POST':
        form = RolePickerForm(request.POST)
        if form.is_valid():
            url_map = {'KONSUMEN': '/register/konsumen/',
                       'ADMIN_APOTEK': '/register/admin_apotek/',
                       'CS': '/register/cs/',
                       'KURIR': '/register/kurir/'}
            return redirect(url_map[form.cleaned_data.get('peran')])
    elif request.method == 'GET':
        form = RolePickerForm()
    context = {'form': form}
    return render(request, "register.html", context)


@no_user
def register_konsumen(request):
    if request.method == 'POST':
        form1 = UserKonsumenForm(request.POST, prefix='user')
        form2 = AddressKonsumenFormSet(request.POST, prefix='address')
        if form1.is_valid() and form2.is_valid():
            form1.save()
            form2.save()
            nama = form1.cleaned_data.get('nama_lengkap')
            input_session(request, form1.cleaned_data.get('email'), nama)
            return redirect('/profile/')
    elif request.method == 'GET':
        form1 = UserKonsumenForm(prefix="user")
        form2 = AddressKonsumenFormSet(prefix="address")
    context = {'form1': form1, 'form2': form2}
    return render(request, "register_konsumen.html", context)


@no_user
def register_admin_apotek(request):
    if request.method == 'POST':
        form = RegisterAdminApotekForm(request.POST)
        if form.is_valid():
            form.save()
            nama = form.cleaned_data.get('nama_lengkap')
            input_session(request, form.cleaned_data.get('email'), nama)
            return redirect('/profile/')
    elif request.method == 'GET':
        form = RegisterAdminApotekForm()
    context = {'form': form}
    return render(request, "register_admin_apotek.html", context)


@no_user
def register_cs(request):
    if request.method == 'POST':
        form = RegisterCSForm(request.POST)
        if form.is_valid():
            form.save()
            nama = form.cleaned_data.get('nama_lengkap')
            input_session(request, form.cleaned_data.get('email'), nama)
            return redirect('/profile/')
    elif request.method == 'GET':
        form = RegisterCSForm()
    context = {'form': form}
    return render(request, "register_cs.html", context)


@no_user
def register_kurir(request):
    if request.method == 'POST':
        form = RegisterKurirForm(request.POST)
        if form.is_valid():
            form.save()
            nama = form.cleaned_data.get('nama_lengkap')
            input_session(request, form.cleaned_data.get('email'), nama)
            return redirect('/profile/')
    form = RegisterKurirForm()
    context = {'form': form}
    return render(request, "register_kurir.html", context)


@user_login_required
def profile(request):
    with connection.cursor() as cursor:
        cursor.execute("SELECT * FROM PENGGUNA WHERE email=%s",
                       [request.session.get("email")])
        user_data = cursor.fetchone()
        role_data = get_role_data(
            request.session.get("email"),
            request.session.get("role"))
    context = {'user': user_data, 'role': role_data}
    if request.session.get('role') == 'KONSUMEN':
        context['data_alamat'] = get_addresses(context['role'][0])
    return render(request, "profile.html", context)

from django.urls import path
from fkauth.views import *

urlpatterns = [
    path('profile/', profile, name="profile"),
    path('register/konsumen/', register_konsumen, name="register_konsumen"),
    path('register/admin_apotek/', register_admin_apotek, name="register_admin_apotek"),
    path('register/cs/', register_cs, name="register_cs"),
    path('register/kurir/', register_kurir, name="register_kurir"),
    path('register/', register, name="register"),
    path('login/', login, name="login"),
    path('logout/', logout, name="logout"),
    path('', home, name="home")
]
import os
import datetime

from django.db import connection
from django.test import TestCase
from django.urls import resolve

from farmakami.settings import BASE_DIR
from fkauth.forms import *
from fkauth.views import *


class FKAuthTestCases(TestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        with connection.cursor() as cursor:
            with open(os.path.join(
                    BASE_DIR,
                    'assets',
                    'sql',
                    'create_sqlite.sql'), 'r') as f:
                for statement in f.read().split("--"):
                    cursor.execute(statement)

    def test_nonexistent_url_maps_to_404(self):
        response = self.client.get("/someillegalurl/")
        self.assertEqual(response.status_code, 404)

    def test_root_url_is_valid(self):
        response = self.client.get("/")
        self.assertEqual(response.status_code, 200)

    def test_root_url_correct_function(self):
        response = resolve('/')
        self.assertEqual(response.func, home)

    def test_root_url_correct_html_template(self):
        response = self.client.get("/")
        self.assertTemplateUsed(response, "index.html")

    def test_login_url_correct_function(self):
        response = resolve("/login/")
        self.assertEqual(response.func, login)

    def test_login_url_not_logged_in_is_valid(self):
        response = self.client.get("/login/")
        self.assertEqual(response.status_code, 200)

    def test_login_url_not_logged_in_correct_html_template(self):
        response = self.client.get("/login/")
        self.assertTemplateUsed(response, "login.html")

    def test_register_url_not_logged_in_is_valid(self):
        response = self.client.get("/register/")
        self.assertEqual(response.status_code, 200)

    def test_register_url_function_used(self):
        response = resolve("/register/")
        self.assertEqual(response.func, register)

    def test_register_url_not_logged_in_correct_template(self):
        response = self.client.get("/register/")
        self.assertTemplateUsed(response, "register.html")

    def test_login_form_email_valid(self):
        test_form = LoginForm({'email': 'a@pk.com', 'password': '123'})
        self.assertTrue(test_form.is_valid())

    def test_login_form_email_not_valid(self):
        test_form = LoginForm({'email': 'haha', 'password': '123456'})
        self.assertFalse(test_form.is_valid())

    def test_login_form_wrong_password(self):
        test_form = LoginForm({'email': 'a@pk.com', 'password': '222992'})
        self.assertFalse(test_form.is_valid())

    def test_login_form_character_too_long(self):
        test_form = LoginForm({'email': ('a'*100)+'@pk.com', 'password': '1'*129})
        self.assertFalse(test_form.is_valid())

    def test_login_form_post_invalid_user(self):
        self.client.post("/login/", data={'email': 'haha@pk.com', 'password': '123456'})
        self.assertIsNone(self.client.session.get('email'))

    def test_login_form_post_wrong_password(self):
        self.client.post("/login/", data={'email': 'a@pk.com', 'password': '192282'})
        self.assertIsNone(self.client.session.get('email'))

    def test_login_form_post_valid_user(self):
        self.client.post("/login/", data={'email': 'a@pk.com', 'password': '123'})
        self.assertEqual(self.client.session['email'], 'a@pk.com')
        self.assertEqual(self.client.session['nama'], 'A')

    def test_login_form_post_valid_role_konsumen(self):
        self.client.post("/login/", data={'email': 'a@pk.com', 'password': '123'})
        self.assertEqual(self.client.session['role'], 'KONSUMEN')

    def test_login_form_post_valid_role_admin_apotek(self):
        self.client.post("/login/", data={'email': 'b@pk.com', 'password': '456'})
        self.assertEqual(self.client.session['role'], 'ADMIN_APOTEK')

    def test_login_form_post_valid_role_cs(self):
        self.client.post("/login/", data={'email': 'c@pk.com', 'password': '789'})
        self.assertEqual(self.client.session['role'], 'CS')

    def test_login_form_post_valid_role_kurir(self):
        self.client.post("/login/", data={'email': 'd@pk.com', 'password': '012'})
        self.assertEqual(self.client.session['role'], 'KURIR')

    def test_login_page_cannot_be_accessed_to_logged_in_user(self):
        self.client.post("/login/", data={'email': 'd@pk.com', 'password': '012'})
        response = self.client.get('/login/')
        self.assertEqual(response.status_code//100, 3)

    def test_logout_url_gives_redirect(self):
        response = self.client.get('/logout/')
        self.assertEqual(response.status_code//100, 3)

    def test_logout_handled_by_correct_function(self):
        response = resolve('/logout/')
        self.assertEqual(response.func, logout)

    def test_logout_clears_user_session(self):
        self.client.post("/login/", data={'email': 'd@pk.com', 'password': '012'})
        self.client.post("/logout/")
        self.assertIsNone(self.client.session.get('email'))

    def test_role_choice_form_valid_value(self):
        form1 = RolePickerForm({'peran': 'KONSUMEN'})
        form2 = RolePickerForm({'peran': 'ADMIN_APOTEK'})
        form3 = RolePickerForm({'peran': 'CS'})
        form4 = RolePickerForm({'peran': 'KURIR'})
        self.assertTrue(form1.is_valid())
        self.assertTrue(form2.is_valid())
        self.assertTrue(form3.is_valid())
        self.assertTrue(form4.is_valid())

    def test_role_choice_form_invalid_value(self):
        form1 = RolePickerForm({'peran': 'ADMINISTRATOR'})
        self.assertFalse(form1.is_valid())

    def test_role_konsumen_redirect_to_correct_form(self):
        response = self.client.post('/register/', data={'peran': 'KONSUMEN'})
        self.assertRedirects(response, '/register/konsumen/')

    def test_role_admin_apotek_redirect_to_correct_form(self):
        response = self.client.post('/register/', data={'peran': 'ADMIN_APOTEK'})
        self.assertRedirects(response, '/register/admin_apotek/')

    def test_role_cs_redirect_to_correct_form(self):
        response = self.client.post('/register/', data={'peran': 'CS'})
        self.assertRedirects(response, '/register/cs/')

    def test_role_kurir_redirect_to_correct_form(self):
        response = self.client.post('/register/', data={'peran': 'KURIR'})
        self.assertRedirects(response, '/register/kurir/')

    def test_register_konsumen_url_is_valid(self):
        response = self.client.get('/register/konsumen/')
        self.assertEqual(response.status_code, 200)

    def test_register_konsumen_function_used(self):
        response = resolve('/register/konsumen/')
        self.assertEqual(response.func, register_konsumen)
    
    def test_register_konsumen_template_used(self):
        response = self.client.get('/register/konsumen/')
        self.assertTemplateUsed(response, "register_konsumen.html")

    def test_register_admin_apotek_url_is_valid(self):
        response = self.client.get('/register/admin_apotek/')
        self.assertEqual(response.status_code, 200)

    def test_register_admin_apotek_function_used(self):
        response = resolve('/register/admin_apotek/')
        self.assertEqual(response.func, register_admin_apotek)

    def test_register_admin_apotek_template_used(self):
        response = self.client.get('/register/admin_apotek/')
        self.assertTemplateUsed(response, "register_admin_apotek.html")
        
    def test_register_cs_url_is_valid(self):
        response = self.client.get('/register/cs/')
        self.assertEqual(response.status_code, 200)

    def test_register_cs_function_used(self):
        response = resolve('/register/cs/')
        self.assertEqual(response.func, register_cs)

    def test_register_cs_template_used(self):
        response = self.client.get('/register/cs/')
        self.assertTemplateUsed(response, "register_cs.html")
        
    def test_register_kurir_url_is_valid(self):
        response = self.client.get('/register/kurir/')
        self.assertEqual(response.status_code, 200)

    def test_register_kurir_function_used(self):
        response = resolve('/register/kurir/')
        self.assertEqual(response.func, register_kurir)

    def test_register_kurir_template_used(self):
        response = self.client.get('/register/kurir/')
        self.assertTemplateUsed(response, "register_kurir.html")

    def test_form_register_admin_apotek_valid_value(self):
        form = RegisterAdminApotekForm({
            'email': 'ab@pk.com',
            'password': '123456',
            'nama_lengkap': 'AB bin CD',
            'nomor_telepon': '0811111'
        })
        self.assertTrue(form.is_valid())

    def test_form_register_admin_apotek_null_value(self):
        form = RegisterAdminApotekForm({
            'email': None,
            'password': None,
            'nama_lengkap': None,
            'nomor_telepon': None
        })
        self.assertFalse(form.is_valid())

    def test_form_register_admin_apotek_too_long(self):
        form = RegisterAdminApotekForm({
            'email': ('a'*100)+'@pk.com',
            'password': '1'*129,
            'nama_lengkap': 'B'*55,
            'nomor_telepon': '0'*21,
        })
        self.assertFalse(form.is_valid())

    def test_form_register_admin_apotek_existing_email(self):
        form = RegisterAdminApotekForm({
            'email': 'b@pk.com',
            'password': '123456',
            'nama_lengkap': 'B bin C',
            'nomor_telepon': '0811111'
        })
        self.assertFalse(form.is_valid())

    def test_register_admin_apotek_valid_redirect(self):
        response = self.client.post(
            '/register/admin_apotek/',
            data={
                'email': 'ab@pk.com',
                'password': '123456',
                'nama_lengkap': 'AB bin CD',
                'nomor_telepon': '0811111'
            })
        self.assertRedirects(response, '/profile/')

    def test_form_register_cs_valid_value(self):
        form = RegisterCSForm({
            'email': 'cd@pk.com',
            'password': '123456',
            'nama_lengkap': 'CD bin EF',
            'nomor_telepon': '0811111',
            'nomor_ktp': '31927219271276231287',
            'nomor_sia': 'SIA19018721928272799'
        })
        self.assertTrue(form.is_valid())

    def test_form_register_cs_null_value(self):
        form = RegisterCSForm({
            'email': None,
            'password': None,
            'nama_lengkap': None,
            'nomor_telepon': None,
            'nomor_ktp': None,
            'nomor_sia': None
        })
        self.assertFalse(form.is_valid())

    def test_form_register_cs_too_long(self):
        form = RegisterCSForm({
            'email': ('a'*100)+'@pk.com',
            'password': '1'*129,
            'nama_lengkap': 'B'*55,
            'nomor_telepon': '0'*21,
            'nomor_ktp': '3'*22,
            'nomor_telepon': '0'*40
        })
        self.assertFalse(form.is_valid())

    def test_form_register_cs_existing_email(self):
        form = RegisterCSForm({
            'email': 'b@pk.com',
            'password': '123456',
            'nama_lengkap': 'B bin C',
            'nomor_telepon': '0811111',
            'nomor_ktp': '31927219271276231287',
            'nomor_sia': 'SIA19018721928272799'
        })
        self.assertFalse(form.is_valid())

    def test_form_register_cs_existing_ktp(self):
        form = RegisterCSForm({
            'email': 'edd@pk.com',
            'password': '123456',
            'nama_lengkap': 'EDD bin C',
            'nomor_telepon': '0811111',
            'nomor_ktp': '31290159741065730119',
            'nomor_sia': 'SIA19018721928272799'
        })
        self.assertFalse(form.is_valid())

    def test_form_register_cs_existing_sia(self):
        form = RegisterCSForm({
            'email': 'd@pk.com',
            'password': '123456',
            'nama_lengkap': 'B bin C',
            'nomor_telepon': '0811111',
            'nomor_ktp': '31927219271276231287',
            'nomor_sia': 'SIA003638553952'
        })
        self.assertFalse(form.is_valid())

    def test_register_cs_valid_redirect(self):
        response = self.client.post(
            '/register/cs/',
            data={
                'email': 'efg@pk.com',
                'password': '123456',
                'nama_lengkap': 'EFG bin CD',
                'nomor_telepon': '0811111',
                'nomor_ktp': '31927219218676231287',
                'nomor_sia': 'SIA19282727272802799'
            })
        self.assertRedirects(response, '/profile/')

    def test_form_register_kurir_valid_value(self):
        form = RegisterKurirForm({
            'email': 'gh@pk.com',
            'password': '123456',
            'nama_lengkap': 'GH bin CD',
            'nomor_telepon': '0811111',
            'nama_perusahaan': 'Sicepat',
        })
        self.assertTrue(form.is_valid())

    def test_form_register_kurir_null_value(self):
        form = RegisterKurirForm({
            'email': None,
            'password': None,
            'nama_lengkap': None,
            'nomor_telepon': None,
            'nomor_ktp': None,
            'nama_perusahaan': None
        })
        self.assertFalse(form.is_valid())

    def test_form_register_kurir_too_long(self):
        form = RegisterKurirForm({
            'email': ('a'*100)+'@pk.com',
            'password': '1'*129,
            'nama_lengkap': 'B'*55,
            'nomor_telepon': '0'*21,
            'nomor_ktp': '3'*22,
            'nama_perusahaan': '0'*40
        })
        self.assertFalse(form.is_valid())

    def test_form_register_kurir_existing_email(self):
        form = RegisterKurirForm({
            'email': 'b@pk.com',
            'password': '123456',
            'nama_lengkap': 'B bin C',
            'nomor_telepon': '0811111',
            'nama_perusahaan': 'Sicepat'
        })
        self.assertFalse(form.is_valid())

    def test_register_kurir_valid_redirect(self):
        response = self.client.post(
            '/register/kurir/',
            data={
                'email': 'gh@pk.com',
                'password': '123456',
                'nama_lengkap': 'GH bin CD',
                'nomor_telepon': '0811111',
                'nama_perusahaan': 'Sicepat',
            })
        self.assertRedirects(response, '/profile/')

    def test_form_user_register_konsumen_valid_values(self):
        form = UserKonsumenForm({
            'email': 'jk@pk.com',
            'password': '123456',
            'nama_lengkap': 'JK bin CD',
            'nomor_telepon': '0811111',
            'jenis_kelamin': 'L',
            'tanggal_lahir': datetime.date.today(),})
        self.assertTrue(form.is_valid())

    def test_form_user_register_konsumen_too_long(self):
        form = UserKonsumenForm({
            'email': 'j'*100,
            'password': 'a'*100,
            'nama_lengkap': 'A'*100,
            'nomor_telepon': '0'*100,
            'jenis_kelamin': 'L'*2,
            'tanggal_lahir': datetime.date.today()})
        self.assertFalse(form.is_valid())

    def test_form_user_register_konsumen_null_value(self):
        form = UserKonsumenForm({
            'email': None,
            'password': None,
            'nama_lengkap': None,
            'nomor_telepon': None,
            'jenis_kelamin': None,
            'tanggal_lahir': None})
        self.assertFalse(form.is_valid())

    def test_form_user_register_konsumen_existing_email(self):
        form = UserKonsumenForm({
            'email': 'b@pk.com',
            'password': '123456',
            'nama_lengkap': 'B bin CD',
            'nomor_telepon': '0811111',
            'jenis_kelamin': 'L',
            'tanggal_lahir': datetime.date.today()})
        self.assertFalse(form.is_valid())

    def test_form_address_register_konsumen_valid_values(self):
        form = AddressKonsumenForm({
            'status': 'Rumah',
            'alamat': 'Sudirman Karet Tanah Abang Duri'
        })
        self.assertTrue(form.is_valid())

    def test_form_address_register_konsumen_too_long(self):
        form = AddressKonsumenForm({
            'status': 'R'*100,
            'alamat': 'Sudirman Karet Tanah Abang Duri'
        })
        self.assertFalse(form.is_valid())

    def test_form_address_register_null_value(self):
        form = AddressKonsumenForm({
            'status': None,
            'alamat': None
        })
        self.assertFalse(form.is_valid())

    def test_form_address_set_single_address(self):
        formset = AddressKonsumenFormSet({
            'form-TOTAL_FORMS': '1',
            'form-INITIAL_FORMS': '0',
            'form-MAX_NUM_FORMS': '',
            'form-0-status': 'Rumah',
            'form-0-alamat': 'Sudirman Karet Tanah Abang Duri'
        })
        self.assertTrue(formset.is_valid())

    def test_form_address_wrong_composing_form(self):
        formset = AddressKonsumenFormSet({
            'form-TOTAL_FORMS': '1',
            'form-INITIAL_FORMS': '0',
            'form-MAX_NUM_FORMS': '',
            'form-0-status': 'R'*100,
            'form-0-alamat': 'Sudirman Karet Tanah Abang Duri'
        })
        self.assertFalse(formset.is_valid())

    def test_form_address_set_multiple_address_distinct(self):
        formset = AddressKonsumenFormSet({
            'form-TOTAL_FORMS': '2',
            'form-INITIAL_FORMS': '0',
            'form-MAX_NUM_FORMS': '',
            'form-0-status': 'Rumah',
            'form-0-alamat': 'Sudirman Karet Tanah Abang Duri',
            'form-1-status': 'Rumah',
            'form-1-alamat': 'Kampung Bandan Jatinegara'
        })
        self.assertTrue(formset.is_valid())

    def test_form_address_set_multiple_address_identical(self):
        formset = AddressKonsumenFormSet({
            'form-TOTAL_FORMS': '2',
            'form-INITIAL_FORMS': '0',
            'form-MAX_NUM_FORMS': '',
            'form-0-status': 'Rumah',
            'form-0-alamat': 'Sudirman Karet Tanah Abang Duri',
            'form-1-status': 'Rumah',
            'form-1-alamat': 'Sudirman Karet Tanah Abang Duri'
        })
        self.assertFalse(formset.is_valid())

    def test_register_konsumen_valid_values_single_address(self):
        response = self.client.post('/register/konsumen/', {
            'user-email': 'jk@pk.com',
            'user-password': '123456',
            'user-nama_lengkap': 'JK bin CD',
            'user-nomor_telepon': '0811111',
            'user-jenis_kelamin': 'L',
            'user-tanggal_lahir': datetime.date.today(),
            'address-TOTAL_FORMS': '1',
            'address-INITIAL_FORMS': '0',
            'address-MAX_NUM_FORMS': '',
            'address-0-status': 'Rumah',
            'address-0-alamat': 'Sudirman Karet Tanah Abang Duri'})
        self.assertRedirects(response, '/profile/')

    def test_register_konsumen_valid_values_multiple_address(self):
        response = self.client.post('/register/konsumen/', {
            'user-email': 'lm@pk.com',
            'user-password': '123456',
            'user-nama_lengkap': 'LM bin CD',
            'user-nomor_telepon': '0811111',
            'user-jenis_kelamin': 'L',
            'user-tanggal_lahir': datetime.date.today(),
            'address-TOTAL_FORMS': '2',
            'address-INITIAL_FORMS': '0',
            'address-MAX_NUM_FORMS': '',
            'address-0-status': 'Rumah',
            'address-0-alamat': 'Sudirman Karet Tanah Abang Duri',
            'address-1-status': 'Rumah',
            'address-1-alamat': 'Kampung Bandan'
        })
        self.assertRedirects(response, '/profile/')

    def test_profile_page_url_no_user_redirects(self):
        response = self.client.get("/profile/")
        self.assertRedirects(response, "/login/")

    def test_profile_page_url_with_login_valid(self):
        self.client.post("/login/", data={'email': 'a@pk.com', 'password': '123'})
        response = self.client.get("/profile/")
        self.assertEqual(response.status_code, 200)

    def test_profile_page_correct_function(self):
        response = resolve("/profile/")
        self.assertEqual(response.func, profile)

    def test_profile_page_with_user_correct_template(self):
        self.client.post("/login/", data={'email': 'a@pk.com', 'password': '123'})
        response = self.client.get("/profile/")
        self.assertTemplateUsed(response, "profile.html")

    def test_register_page_cannot_be_accessed_by_logged_in_user(self):
        self.client.post("/login/", data={'email': 'a@pk.com', 'password': '123'})
        response = self.client.get("/register/")
        self.assertRedirects(response, '/profile/')

    def test_register_admin_apotek_page_cannot_be_accessed_by_logged_in_user(self):
        self.client.post("/login/", data={'email': 'a@pk.com', 'password': '123'})
        response = self.client.get("/register/admin_apotek/")
        self.assertRedirects(response, '/profile/')

    def test_register_cs_page_cannot_be_accessed_by_logged_in_user(self):
        self.client.post("/login/", data={'email': 'a@pk.com', 'password': '123'})
        response = self.client.get("/register/cs/")
        self.assertRedirects(response, '/profile/')

    def test_register_kurir_page_cannot_be_accessed_by_logged_in_user(self):
        self.client.post("/login/", data={'email': 'a@pk.com', 'password': '123'})
        response = self.client.get("/register/kurir/")
        self.assertRedirects(response, '/profile/')

    def test_register_konsumen_page_cannot_be_accessed_by_logged_in_user(self):
        self.client.post("/login/", data={'email': 'a@pk.com', 'password': '123'})
        response = self.client.get("/register/konsumen/")
        self.assertRedirects(response, '/profile/')

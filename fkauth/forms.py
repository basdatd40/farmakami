from django import forms
from django.db import connection
from passlib.hash import pbkdf2_sha256
from random import choice


class LoginForm(forms.Form):
    email = forms.EmailField(
        widget=forms.EmailInput(attrs={
            'class': 'form-control',
            'placeholder': 'example@email.com'}),
        max_length=50)
    password = forms.CharField(
        widget=forms.PasswordInput(attrs={'class': 'form-control'}),
        max_length=128)

    def clean(self):
        cleaned_data = super().clean()
        email = cleaned_data.get('email')
        password = cleaned_data.get('password')
        with connection.cursor() as cursor:
            cursor.execute(
                "SELECT * FROM PENGGUNA WHERE email=%s LIMIT 1;",
                [email])
            data = cursor.fetchone()
        if data is None:
            self.add_error('email', 'Email tidak terdaftar')
        elif not pbkdf2_sha256.verify(password, data[2]):
            self.add_error('password', 'Password salah')


class RolePickerForm(forms.Form):
    ROLE_CHOICE = [('KONSUMEN', 'Konsumen'),
                   ('ADMIN_APOTEK', 'Admin Apotek'),
                   ('CS', 'Customer Service'),
                   ('KURIR', 'Kurir')]
    peran = forms.ChoiceField(choices=ROLE_CHOICE,
                              widget=forms.Select(attrs={'class': 'form-control'}))


class RegisterUserForm(forms.Form):
    email = forms.EmailField(
        widget=forms.EmailInput(attrs={
            'class': 'form-control'}),
        max_length=50)
    password = forms.CharField(
        widget=forms.PasswordInput(attrs={'class': 'form-control'}),
        max_length=128)
    nama_lengkap = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control'}),
        max_length=50
    )
    nomor_telepon = forms.CharField(
        widget=forms.NumberInput(attrs={
            'class': 'form-control',
            'placeholder': '0xx (w/o country code)'}),
        max_length=20
    )

    def clean_email(self):
        data = self.cleaned_data.get('email')
        with connection.cursor() as cursor:
            cursor.execute("SELECT * FROM PENGGUNA WHERE email=%s LIMIT 1",
                           [data])
            row = cursor.fetchone()
        if row is not None:
            raise forms.ValidationError("Email sudah terdaftar")
        return data

    def clean_password(self):
        data = self.cleaned_data.get('password')
        data = pbkdf2_sha256.hash(data, rounds=200000, salt_size=16)
        return data

    def save(self):
        email = self.cleaned_data.get('email')
        password = self.cleaned_data.get('password')
        nomor_telepon = self.cleaned_data.get('nomor_telepon')
        nama_lengkap = self.cleaned_data.get('nama_lengkap')

        with connection.cursor() as cursor:
            cursor.execute("INSERT INTO PENGGUNA VALUES(%s,%s,%s,%s)",
                           [email, nomor_telepon, password, nama_lengkap])


class RegisterAdminApotekForm(RegisterUserForm):
    def save(self):
        super().save()
        email = self.cleaned_data.get('email')
        with connection.cursor() as cursor:
            cursor.execute("SELECT id_apotek FROM apotek")
            apotek = cursor.fetchall()
            new_id = choice(apotek)[0]
            cursor.execute("INSERT INTO APOTEKER VALUES(%s)", [email])
            cursor.execute("INSERT INTO ADMIN_APOTEK VALUES(%s, %s)", [email, new_id])


class RegisterCSForm(RegisterUserForm):
    nomor_ktp = forms.CharField(
        widget=forms.NumberInput(attrs={'class': 'form-control'}),
        max_length=20
    )
    nomor_sia = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control'}),
        max_length=20
    )

    def clean_nomor_ktp(self):
        data = self.cleaned_data.get('nomor_ktp')
        with connection.cursor() as cursor:
            cursor.execute("SELECT * FROM CS WHERE no_ktp=%s LIMIT 1",
                           [data])
            row = cursor.fetchone()
        if row is not None:
            raise forms.ValidationError("Nomor KTP sudah terdaftar")
        return data

    def clean_nomor_sia(self):
        data = self.cleaned_data.get('nomor_sia')
        with connection.cursor() as cursor:
            cursor.execute("SELECT * FROM CS WHERE no_sia=%s LIMIT 1",
                           [data])
            row = cursor.fetchone()
        if row is not None:
            raise forms.ValidationError("Nomor SIA sudah terdaftar")
        return data

    def save(self):
        super().save()
        email = self.cleaned_data.get('email')
        no_ktp = self.cleaned_data.get('nomor_ktp')
        no_sia = self.cleaned_data.get('nomor_sia')
        with connection.cursor() as cursor:
            cursor.execute("INSERT INTO APOTEKER VALUES(%s)", [email])
            cursor.execute("INSERT INTO CS VALUES(%s,%s,%s)",
                           [no_ktp, email, no_sia])


class RegisterKurirForm(RegisterUserForm):
    nama_perusahaan = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control'}),
        max_length=50)

    def save(self):
        super().save()
        email = self.cleaned_data.get('email')
        nama_perusahaan = self.cleaned_data.get('nama_perusahaan')
        with connection.cursor() as cursor:
            cursor.execute("SELECT * FROM KURIR ORDER BY id_kurir DESC LIMIT 1")
            new_id_number = int(cursor.fetchone()[0][3:])+1
            new_id = f"FKK{new_id_number:07}"
            cursor.execute("INSERT INTO KURIR VALUES(%s,%s,%s)",
                           [new_id, email, nama_perusahaan])


class UserKonsumenForm(RegisterUserForm):
    GENDER_CHOICES = [
        ('L', 'Laki-Laki'),
        ('P', 'Perempuan'),
        ('U', 'Tidak Ingin Memberitahu')
    ]
    jenis_kelamin = forms.CharField(
        widget=forms.Select(
            choices=GENDER_CHOICES,
            attrs={'class': 'form-control'},),
        max_length=1,
    )
    tanggal_lahir = forms.DateField(
        widget=forms.DateInput(
            attrs={'class': 'form-control',
                   'placeholder': 'Format: mm/dd/yyyy'}),
    )

    def save(self):
        super().save()
        email = self.cleaned_data.get('email')
        jenis_kelamin = self.cleaned_data.get('jenis_kelamin')
        tanggal_lahir = self.cleaned_data.get('tanggal_lahir')
        with connection.cursor() as cursor:
            cursor.execute("SELECT * FROM KONSUMEN ORDER BY id_konsumen DESC LIMIT 1")
            new_id_number = int(cursor.fetchone()[0][3:]) + 1
            new_id = f"FKC{new_id_number:07}"
            cursor.execute("INSERT INTO KONSUMEN VALUES(%s,%s,%s,%s)",
                           [new_id, email, jenis_kelamin, tanggal_lahir])


class AddressKonsumenForm(forms.Form):
    status = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control'}),
        max_length=20,
    )
    alamat = forms.CharField(
        widget=forms.Textarea(attrs={
            'class': 'form-control',
            'rows': 1})
    )

    def save(self):
        status = self.cleaned_data.get('status')
        alamat = self.cleaned_data.get('alamat')
        with connection.cursor() as cursor:
            cursor.execute("SELECT id_konsumen FROM konsumen ORDER BY id_konsumen DESC LIMIT 1")
            current_id = cursor.fetchone()[0]
            cursor.execute("INSERT INTO alamat_konsumen VALUES(%s,%s,%s)",
                           [current_id, alamat, status])


class BaseAddressKonsumenFormSet(forms.BaseFormSet):
    def clean(self):
        super().clean()
        if any(self.errors):
            return None
        alamat_list = []
        for form in self.forms:
            status = form.cleaned_data.get('status')
            alamat = form.cleaned_data.get('alamat')
            if (status, alamat) in alamat_list:
                raise forms.ValidationError("Terdapat alamat ganda")
            alamat_list.append((status, alamat))

    def save(self):
        for form in self.forms:
            form.save()


AddressKonsumenFormSet = forms.formset_factory(
    AddressKonsumenForm,
    formset=BaseAddressKonsumenFormSet)
